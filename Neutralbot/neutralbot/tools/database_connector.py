import os
import sys

# import pymssql
from datetime import datetime, timezone
from django.db import connection
from main.models import Submission


class NonUniqueValueError(Exception):
    """Raised when the returned results in not unique but should be"""
    pass


def get_connection():
    return connection
#    connection = pymssql.connect(
#        host=os.getenv('REDDIT_DB_HOST') or sys.argv[2],
#        user=os.getenv('REDDIT_DB_USER') or sys.argv[3],
#        password=os.getenv('REDDIT_DB_PASSWORD') or sys.argv[4],
#        database=os.getenv('REDDIT_DB_DATABASE') or sys.argv[5])
#    return connection


def check_unique_result(results, data_name, data_id):
    if 0 == len(results):
        return None
    elif 1 == len(results):
        return results[0]
    else:
        raise NonUniqueValueError('Non unique ' + data_name + ' ' + data_id)


def get_results_dic(query):
    """Takes in a query string and converts the result into a list of dictionary values"""
    conn = get_connection()
    cursor = conn.cursor()
    print('executing query:', query)
    cursor.execute(query)
    if cursor.description is None:
        return []
    results = [dict(zip([column[0] for column in cursor.description], row))
               for row in cursor.fetchall()]
    cursor.close()
    conn.close()
    return results


def update_rows(query):
    """Takes in a query string and converts the result into a list of dictionary values"""
    conn = get_connection()
    cursor = conn.cursor()
    print('executing query', query)
    cursor.execute(query)
    conn.commit()
    cursor.close()
    conn.close()


def insert_rows(table, rows):
    insert_query = 'INSERT INTO ' + table + '('
    values_string = 'VALUES ('
    sep = ''

    for column in rows[0]:
        insert_query += sep + column
        values_string += sep + '%s'
        sep = ', '

    inserts = []
    for row in rows:
        values_list = []
        for value in row.values():
            values_list.append(value)
        inserts.append(tuple(values_list))

    insert_query += ')\n' + values_string + ')'
    conn = get_connection()
    cursor = conn.cursor()
    print('executing query', insert_query, inserts)
    cursor.executemany(insert_query, inserts)
    conn.commit()
    cursor.close()
    conn.close()


def get_subreddit(subreddit_id):
    query = r"SELECT * FROM neutralverse_NeutralverseSubreddit where T5Id = '" + subreddit_id + "'"
    results = get_results_dic(query)
    return check_unique_result(results, 'subreddit', subreddit_id)


def create_subreddit(subreddit, user_name='NeutralverseBot'):
    # sets to NeutralverseBot as creator by default
    user_id = get_user(user_name)["RedditUserId"]
    subreddit_data = [{'Name': subreddit.display_name, 'T5Id': subreddit.id, 'IsEnabled': True,
                       'CreatedBy': user_id, 'UpdatedBy': user_id}]
    insert_rows('neutralverse_NeutralverseSubreddit', subreddit_data)
    return subreddit_data[0]


def get_subreddit_or_create(subreddit):
    subreddit_data = get_subreddit(subreddit.id)
    if subreddit_data is None:
        return create_subreddit(subreddit)
    else:
        return subreddit_data


def get_rule(rule_name, subreddit_id):
    query = r"SELECT * FROM moderation_RuleType RT " \
            r"join neutralverse_NeutralverseSubreddit NS on NS.SubredditId = Rt.SubredditId " \
            r"where NS.T5Id = '" + subreddit_id + "' and RT.Name = '" + rule_name + "' "
    results = get_results_dic(query)
    return check_unique_result(results, 'rule', rule_name)


def get_rules(subreddit_id):
    query = r"SELECT RT.* FROM moderation_RuleType RT " \
            r"join neutralverse_NeutralverseSubreddit NS on NS.SubredditId = Rt.SubredditId " \
            r"where NS.T5Id = '" + subreddit_id + "' "
    return get_results_dic(query)


def update_rule(rule_type_id, rule_data, current_version):
    """
    :param rule_type_id: an integer like 12
    :param rule_data: a dict like {'SubredditId': 11, 'Name': 'Rule 2', 'Points': 20}
    :param current_version: the most recent database version for the rule
    """
    query = r"UPDATE moderation_RuleType " \
            r"SET SubredditId = " + str(rule_data['SubredditId']) + r", " \
            r"Name = '" + rule_data['Name'] + r"', " \
            r"Points = " + str(rule_data['Points']) + r", " \
            r"Version = " + str(current_version + 1) + r" " \
            r"WHERE RuleTypeId = " + str(rule_type_id)
    print('update_rule query', query)
    return update_rows(query)


def update_rules(rule_list, subreddit):
    """
    :param rules_list: this is raw data direct from the wiki config,
        it's a list of rules like:

        [{'Name': 'Rule 1', 'Points': 100},
         {'Name': 'Rule 2', 'Points': 20},
         {'Name': 'Rule 3', 'Points': 25},
         {'Name': 'Rule 4', 'Points': 30}]
    :param subreddit: a Praw Subreddit object, like Subreddit(display_name='neutralnews')
    """
    def update_rule_in_list():
        for db_rule in db_rules_data:
            if db_rule['Name'] == rule['Name']:
                if not (db_rule['Points'] == rule['Points']):
                    update_rule(db_rule['RuleTypeId'], rule_data, db_rule['Version'])
                return True
        return False
    db_rules_data = get_rules(subreddit.id)
    # db_rules_data contains a list of dicts like
    # {'Name': 'Rule 1',
    #  'Points': 100,
    #  'RuleTypeId': 11,
    #  'SubredditId': 11,
    #  'Version': 1},
    # note that only the most recent version will be in the list, and
    # the Version key will be populated
    subreddit_data = get_subreddit_or_create(subreddit)
    for rule in rule_list:
        rule_data = {'SubredditId': subreddit_data['SubredditId'], 'Name': rule['Name'], 'Points': rule['Points']}
        if not update_rule_in_list():
            insert_rows('moderation_RuleType', [rule_data])


def get_violations_settings(subreddit_id):
    query = r"SELECT VS.* FROM moderation_ViolationsSettings VS " \
            r"join neutralverse_NeutralverseSubreddit NS on NS.SubredditId = VS.SubredditId " \
            r"where NS.T5Id = '" + subreddit_id + "' "
    results = get_results_dic(query)
    return check_unique_result(results, 'ViolationsSettings', subreddit_id)


def update_violations_settings(violations_settings, subreddit):
    db_violations_settings_data = get_violations_settings(subreddit.id)
    if db_violations_settings_data is None:
        subreddit_data = get_subreddit_or_create(subreddit)
        violations_settings_data = [{'TrackDurationDays': violations_settings['TrackDurationDays'],
                                    'RestoreRefundPercent': violations_settings['RestoreRefundPercent'],
                                    'DeleteRefundPercent': violations_settings['DeleteRefundPercent'],
                                    'AggregateViolationsInComment': violations_settings['AggregateViolationsInComment'],
                                    'AggregateViolationsInChain': violations_settings['AggregateViolationsInChain'],
                                    'SubredditId': subreddit_data['SubredditId']}]
        return insert_rows('moderation_ViolationsSettings', violations_settings_data)
    else:
        aggregate_comment = '0'
        aggregate_chain = '0'
        if violations_settings['AggregateViolationsInComment']:
            aggregate_comment = '1'
        if violations_settings['AggregateViolationsInChain']:
            aggregate_chain = '1'
        query = r"UPDATE moderation_ViolationsSettings " \
                r"SET TrackDurationDays = " + str(violations_settings['TrackDurationDays']) + r", " \
                r"RestoreRefundPercent = " + str(violations_settings['RestoreRefundPercent']) + r", " \
                r"DeleteRefundPercent = " + str(violations_settings['DeleteRefundPercent']) + r", " \
                r"AggregateViolationsInComment = " + aggregate_comment + r", " \
                r"AggregateViolationsInChain = " + aggregate_chain + r" " \
                r"where ViolationsSettingsId = " + str(db_violations_settings_data['ViolationsSettingsId'])
        return update_rows(query)


def get_user(user_name):
    print(f'get_user({user_name})')
    query = r"SELECT RU.* FROM neutralverse_RedditUser RU where RU.UserName = '" + user_name + "'"
    results = get_results_dic(query)
    return check_unique_result(results, 'user', user_name)


def create_user(user):
    user_data = [{'Username': user.name, 'T2Id': '', 'T5Id': '', 'HasSystemAccess': False, 'IsSystemUser': False}]
    insert_rows('neutralverse_RedditUser', user_data)
    return user_data


def get_user_or_create(user):
    user_data = get_user(user.name)
    if user_data is None:
        create_user(user)
        user_data = get_user(user.name)
    return user_data


def get_submission(submission_id):
    query = r"SELECT * FROM neutralverse_Submission where T3Id = '" + submission_id + "'"
    results = get_results_dic(query)
    return check_unique_result(results, 'submission', submission_id)


def create_submission(submission, removed=False):
    subreddit_id = get_subreddit_or_create(submission.subreddit)["SubredditId"]
    user_id = get_user_or_create(submission.author)["RedditUserId"]
    created_dt_tm = datetime.fromtimestamp(submission.created_utc).strftime("%Y-%m-%d %H:%M:%S")
    submission_data = [
        {'SubredditId': subreddit_id, 'RedditUserId': user_id, 'T3Id': submission.id, 'Permalink': submission.permalink,
         'IsRemoved': removed, 'SubmittedDtTm': created_dt_tm}]
    insert_rows('neutralverse_Submission', submission_data)


def get_submission_or_create(submission, removed=False):
    submission_data = get_submission(submission.id)
    if submission_data is None:
        create_submission(submission, removed)
        submission_data = get_submission(submission.id)
    return submission_data


def get_submission_cnt(user_name, subreddit_id, lookback_days=None):
    """
    :param lookback_days: comes from post_limits["days"] in the wiki config, is probably an integer or None
    """
    lookback_days_qual = ''
    if lookback_days is not None:
        # new sqlite
        lookback_days_qual = f"and CURRENT_TIMESTAMP <= datetime(S.SubmittedDtTm, '+{lookback_days} day') "
        # original mssql
        #lookback_days_qual = 'and GETUTCDATE() <= dateadd(day, ' + str(lookback_days) + ', S.SubmittedDtTm) '
    query = r"SELECT count(distinct S.T3Id) AS COUNT FROM neutralverse_Submission S " \
            r"join neutralverse_RedditUser RU on RU.RedditUserId = S.RedditUserId " \
            r"JOIN neutralverse_NeutralverseSubreddit NS ON NS.SubredditId = S.SubredditId " \
            r"where S.IsRemoved = 0 " \
            r"and NS.T5Id = '" + subreddit_id + "' " \
            r"and RU.UserName = '" + user_name + "' " + lookback_days_qual
    res = get_results_dic(query)
    return res[0]['COUNT']


def get_next_post_date(user_name, subreddit_id, lookback_days):

#    -    query = r"SELECT TOP 1 dateadd(day, 7, S.SubmittedDtTm) FROM neutralverse.Submission S " \
#                        r"join neutralverse.RedditUser RU on RU.RedditUserId = S.RedditUserId " \
#                        r"JOIN neutralverse.NeutralverseSubreddit NS ON NS.SubredditId = S.SubredditId " \
#                        r"where S.IsRemoved = 0 " \
#                        r"and NS.T5Id = '" + subreddit_id + "' " \
#                        r"and RU.UserName = '" + user_name + "' " \
#                        r"and GETUTCDATE() <= dateadd(day, " + str(lookback_days) + ", S.SubmittedDtTm) " \
#                        r"order by S.SubmittedDtTm"


    # original mssql
#     query = r"SELECT dateadd(day, 7, S.SubmittedDtTm) FROM neutralverse_Submission S " \
#             r"join neutralverse_RedditUser RU on RU.RedditUserId = S.RedditUserId " \
#             r"JOIN neutralverse_NeutralverseSubreddit NS ON NS.SubredditId = S.SubredditId " \
#             r"where S.IsRemoved = 0 " \
#             r"and NS.T5Id = '" + subreddit_id + "' " \
#             r"and RU.UserName = '" + user_name + "' " \
#             r"and GETUTCDATE() <= dateadd(day, " + str(lookback_days) + ", S.SubmittedDtTm) " \
#             r"order by S.SubmittedDtTm limit 1"
#     query_result = get_results_dic(query)[0]
# get all the submissions they have made within the 7 (currently) day window, find the earliest of them, and then add 7 days to find when they can submit next
    from django.utils import timezone
    from datetime import timedelta
    window = timezone.now() - timedelta(days=lookback_days)
    submission = Submission.objects.filter(
        reddituser__username=user_name,
        subreddit__t5id=subreddit_id,
        isremoved=False,
        submitteddttm__gte=window
    ).first()
    return submission.submitteddttm + timedelta(days=lookback_days)


def get_comment(user_comment_id):
    query = r"SELECT * FROM neutralverse_SubmissionComment where T1Id = '" + user_comment_id + "'"
    results = get_results_dic(query)
    return check_unique_result(results, 'comment', user_comment_id)


def create_comment(user_comment, first_comment):
    first_comment_id = first_comment.id
    subreddit_id = get_subreddit_or_create(user_comment.subreddit)["SubredditId"]
    user_id = get_user_or_create(user_comment.author)["RedditUserId"]
    created_dt_tm = datetime.fromtimestamp(user_comment.created_utc).strftime("%Y-%m-%d %H:%M:%S")
    comment_data = [{'SubredditId': subreddit_id, 'RedditUserId': user_id, 'FirstCommentId': first_comment_id,
                     'T1Id': user_comment.id, 'Permalink': user_comment.permalink, 'SubmittedDtTm': created_dt_tm}]
    insert_rows('neutralverse_SubmissionComment', comment_data)


def get_comment_or_create(user_comment, first_comment):
    comment_data = get_comment(user_comment.id)
    if comment_data is None:
        create_comment(user_comment, first_comment)
        comment_data = get_comment(user_comment.id)
    return comment_data


def get_rules_violated(comment_id):
    query = r"SELECT RT.*, RVH.IsCommentModRestored FROM moderation_RuleType RT " \
            r"join neutralverse_RuleViolationHist RVH on RVH.RuleTypeId = RT.RuleTypeId " \
            r"join neutralverse_SubmissionComment SC on RVH.SubmissionCommentId = SC.SubmissionCommentId " \
            r"where SC.T1ID = '" + comment_id + r"' "
    return get_results_dic(query)


def get_rule_violation(user_comment, first_comment, broken_rule_name):
    comment_data = get_comment_or_create(user_comment, first_comment)
    rule_type_id = get_rule(broken_rule_name, user_comment.subreddit.id)["RuleTypeId"]
    comment_id = comment_data["T1Id"]
    query = r"SELECT RVH.* FROM neutralverse_SubmissionComment SC " \
            r"join neutralverse_RuleViolationHist RVH on RVH.SubmissionCommentId = SC.SubmissionCommentId " \
            r"join neutralverse_SubmissionComment SC2 on SC2.T1Id = SC.T1Id " \
            r"where SC.T1ID = '" + comment_id + r"' " \
            r"and RVH.RuleTypeId = '" + str(rule_type_id) + "'"
    results = get_results_dic(query)
    return check_unique_result(results, "rule_violation_hist", user_comment.id)


def create_rule_violation_hist(user_comment, broken_rule_name, broken_rule_type_version, first_comment, mod_comment):
    comment_data = get_comment_or_create(user_comment, first_comment)
    rule_type_id = get_rule(broken_rule_name, user_comment.subreddit.id)["RuleTypeId"]
    submission_comment_id = comment_data["SubmissionCommentId"]
    mod_user_id = get_user_or_create(mod_comment.author)["RedditUserId"]
    rule_violation_hist_data = [{'RuleTypeId': rule_type_id, 'RuleTypeVersion': broken_rule_type_version,
                                 'SubmissionCommentId': submission_comment_id, 'ModUserId': mod_user_id,
                                 'ModT1Id': mod_comment.id}]
    insert_rows('neutralverse_RuleViolationHist', rule_violation_hist_data)


def get_related_violations(comment_id):
    query = r"SELECT SC.* FROM neutralverse_SubmissionComment SC " \
            r"join neutralverse_RuleViolationHist RVH on RVH.SubmissionCommentId = SC.SubmissionCommentId " \
            r"join neutralverse_SubmissionComment SC2 on SC2.FirstCommentId = SC.FirstCommentId " \
            r"where SC.T1ID = '" + comment_id + r"' " \
            r"and SC2.SubmissionCommentId != SC.SubmissionCommentId"
    return get_results_dic(query)


def get_active_ban_vote(user_name, subreddit_id):
    subreddit_data = get_subreddit(subreddit_id)
    user_data = get_user(user_name)
    query = r"SELECT BV.* from neutralverse_BanVote BV " \
            r"where BV.ActiveStatusFlag = 1 and " \
            r"BV.RedditUserId = '" + str(user_data['RedditUserId']) + "' " \
            r"and BV.SubredditId = " + str(subreddit_data['SubredditId'])
    results = get_results_dic(query)
    return check_unique_result(results, "user_name", user_name)


def set_ban_vote_active_ind(ban_vote_id, active_ind):
    query = r"UPDATE neutralverse_BanVote " \
            r"SET ActiveStatusFlag = " + str(int(active_ind)) + " " + \
            r"WHERE BanVoteId = " + str(ban_vote_id)
    update_rows(query)


def create_ban_hist(ban_type_id, ban_type_version, ban_vote_id, user_name):
    ban_dt_tm = datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
    user_data = get_user(user_name)
    ban_hist_data = [{'BanTypeId': ban_type_id, 'BanTypeVersion': ban_type_version,
                      'RedditUserId': user_data['RedditUserId'], 'BanVoteId': ban_vote_id, 'BanDtTm': ban_dt_tm}]
    insert_rows('neutralverse_BanHist', ban_hist_data)


def get_ban_vote_for_ban(ban_hist):
    if ban_hist is None:
        return None
    query = r"SELECT BV.* from neutralverse_BanVote BV" \
            r"join neutralverse_banhist BH on BH.BanVoteId = BV.BanVoteId" \
            r"where BH.BanHistId = " + ban_hist["BanHistId"]
    results = get_results_dic(query)
    return check_unique_result(results, "ban_vote", ban_hist["BanHistId"])


def get_relevant_ban_votes(user, previous_ban):
    previous_ban_dt_tm_qual = ''
    if previous_ban is not None:
        previous_ban_dt_tm = datetime.strftime(previous_ban["BanDtTm"], "%Y-%m-%d %H:%M:%S")
        previous_ban_dt_tm_qual = r" and BV.StartDtTm > '" + previous_ban_dt_tm + r"' "
    query = r"SELECT BV.* from neutralverse_BanVote BV " \
            r"join neutralverse_RedditUser RU on RU.RedditUserId = BV.RedditUserId " \
            r"where RU.UserName = '" + user.name + "' " + previous_ban_dt_tm_qual + " " \
            r"ORDER BY BV.StartDtTm DESC"
    return get_results_dic(query)


def create_ban_vote(modmail, user, ban_type_id, ban_type_version):
    subreddit_id = get_subreddit_or_create(modmail.subreddit)["SubredditId"]
    user_id = get_user_or_create(user)["RedditUserId"]
    submitted_dt_tm = datetime.fromtimestamp(modmail.created_utc).strftime("%Y-%m-%d %H:%M:%S")
    t3_id = modmail.id
    permalink = '/message/messages/' + t3_id
    ban_vote_data = [{'SubredditId': subreddit_id, 'RedditUserId': user_id, 'BanTypeId': ban_type_id,
                      'BanTypeVersion': ban_type_version, 'ModmailPermalink': permalink, 'T3Id': t3_id,
                      'StartDtTm': submitted_dt_tm}]
    insert_rows('neutralverse_BanVote', ban_vote_data)


def get_previous_ban(user, subreddit_id):
    query = r"SELECT BTH.*, BH.BanDtTm FROM neutralverse_BanHist BH " \
            r"join neutralverse_RedditUser RU on RU.RedditUserId = BH.RedditUserId " \
            r"join moderation_BanTypeHist BTH on BTH.BanTypeId = BH.BanTypeId " \
            r"and BTH.Version = BH.BanTypeVersion " \
            r"join neutralverse_NeutralverseSubreddit NS on NS.SubredditId = BTH.SubredditId " \
            r"where RU.UserName = '" + user.name + "' " \
            r"and NS.T5Id = '" + subreddit_id + "' " \
            r"order by BH.BanDtTm desc limit 1"
    results = get_results_dic(query)
    return check_unique_result(results, 'bans in effect for user', user.name)


def get_ban_type(ban_type_id, ban_type_version):
    query = r"SELECT BTH.* FROM moderation_BanTypeHist BTH " \
            r"where BTH.BanTypeId = " + str(ban_type_id) + r" " \
            r"and BTH.Version = " + str(ban_type_version)
    results = get_results_dic(query)
    return check_unique_result(results, 'ban_type_id', ban_type_id)


def get_ban_types(subreddit_id):
    query = r"SELECT BT.* FROM moderation_BanType BT " \
            r"join neutralverse_NeutralverseSubreddit NS on NS.SubredditId = BT.SubredditId " \
            r"where NS.T5Id = '" + subreddit_id + "' "
    return get_results_dic(query)


def get_next_ban_type(previous_ban, subreddit_id):
    if previous_ban is None:
        first_ban_query = r"SELECT * from moderation_BanType BT " \
                          r"join neutralverse_NeutralverseSubreddit NS on NS.SubredditId = BT.SubredditId " \
                          r" where BT.PriorityOrder = 1 " \
                          r" and NS.T5Id = '" + subreddit_id + r"'"
        tracking_ban = get_results_dic(first_ban_query)
    else:
        next_ban_priority = previous_ban["PriorityOrder"] + 1
        next_ban_query = r"SELECT * from moderation_BanType " \
                         r"where PriorityOrder = " + str(next_ban_priority) + r" "\
                         r"and SubredditId = " + str(previous_ban["SubredditId"])
        tracking_ban = get_results_dic(next_ban_query)

    return check_unique_result(tracking_ban, 'ban_type_id', previous_ban)


def update_ban_type(ban_type_id, ban_type_data):
    automatic = '0'
    if ban_type_data['IsAutomatic']:
        automatic = '1'
    query = r"UPDATE moderation_BanType " \
            r"SET SubredditId = " + str(ban_type_data['SubredditId']) + r", " \
            r"BanLengthDays = " + str(ban_type_data['BanLengthDays']) + r", " \
            r"PriorityOrder = " + str(ban_type_data['PriorityOrder']) + r", " \
            r"PointsThreshold = " + str(ban_type_data['PointsThreshold']) + r", " \
            r"IsAutomatic = " + automatic + ", " \
            r"Version = Version + 1 " \
            r"WHERE BanTypeId = " + str(ban_type_id)
    return update_rows(query)


def update_ban_types(ban_type_list, subreddit):
    def update_ban_type_in_list():
        for list_ban_type in db_ban_type_data:
            if list_ban_type['PriorityOrder'] == ban_type['PriorityOrder']:
                if not (list_ban_type['BanLengthDays'] == ban_type['BanLengthDays']
                        and list_ban_type['PointsThreshold'] == ban_type['PointsThreshold']
                        and list_ban_type['IsAutomatic'] == ban_type['IsAutomatic']):
                    update_ban_type(list_ban_type['BanTypeId'], ban_type_data)
                return True
        return False
    db_ban_type_data = get_ban_types(subreddit.id)
    subreddit_data = get_subreddit_or_create(subreddit)
    for ban_type in ban_type_list:
        ban_type_data = {'SubredditId': subreddit_data['SubredditId'], 'BanLengthDays': ban_type['BanLengthDays'],
                         'PriorityOrder': ban_type['PriorityOrder'], 'PointsThreshold': ban_type['PointsThreshold'],
                         'IsAutomatic': ban_type['IsAutomatic']}
        if not update_ban_type_in_list():
            insert_rows('moderation_BanType', [ban_type_data])


def get_user_list():
    query = r"select distinct RU.username from neutralverse_RedditUser RU join neutralverse_SubmissionComment SC on SC.RedditUserId = RU.RedditUserId where SC.SubredditId = 11"
    return get_results_dic(query)


# def get_rule_violations(user, subreddit_id, track_days=0):
#     query = r"SELECT RTH.Name, RTH.Points, RVH.IsCommentModRestored, RUMod.UserName AS ModUserName, SC.* "\
#             r"FROM neutralverse_RuleViolationHist RVH " \
#             r"join neutralverse_SubmissionComment SC on SC.SubmissionCommentId = RVH.SubmissionCommentId " \
#             r"join neutralverse_NeutralverseSubreddit NS on NS.SubredditId = SC.SubredditId " \
#             r"join neutralverse_RedditUser RU on RU.RedditUserId = SC.RedditUserId " \
#             r"join neutralverse_RedditUser RUMod on RUMod.RedditUserId = RVH.ModUserId " \
#             r"join moderation_RuleTypeHist RTH on RTH.RuleTypeId = RVH.RuleTypeId " \
#             r" and RTH.Version = RVH.RuleTypeVersion " \
#             r"where RU.UserName = '" + user.name + "' " \
#             r"and GETUTCDATE() <= dateadd(day," + str(track_days) + " , SC.SubmittedDtTm) " \
#             r"and SC.SubmittedDtTm >(" \
#             r"select coalesce(max(BH.BanDtTm), 0) as LastBanDtTm " \
#             r"from neutralverse_BanHist BH where BH.RedditUserId = RU.RedditUserId ) " \
#             r"and NS.T5Id = '" + subreddit_id + "' " \
#             r"order by SC.FirstCommentId desc, SC.SubmittedDtTm desc, RTH.Points desc "
#     return get_results_dic(query)

def get_rule_violations(user, subreddit_id, track_days=0):
    query = r"SELECT RTH.Name, RTH.Points, RVH.IsCommentModRestored, RUMod.UserName AS ModUserName, SC.* "\
            r"FROM neutralverse_RuleViolationHist RVH " \
            r"JOIN neutralverse_SubmissionComment SC on SC.SubmissionCommentId = RVH.SubmissionCommentId " \
            r"JOIN neutralverse_NeutralverseSubreddit NS on NS.SubredditId = SC.SubredditId " \
            r"JOIN neutralverse_RedditUser RU on RU.RedditUserId = SC.RedditUserId " \
            r"JOIN neutralverse_RedditUser RUMod on RUMod.RedditUserId = RVH.ModUserId " \
            r"JOIN moderation_RuleTypeHist RTH on RTH.RuleTypeId = RVH.RuleTypeId " \
            r"AND RTH.Version = RVH.RuleTypeVersion " \
            r"WHERE RU.UserName = '" + user.name + "' " \
            r"AND CURRENT_TIMESTAMP <= datetime(SC.SubmittedDtTm, '+" + str(track_days) + " days') " \
            r"AND SC.SubmittedDtTm >(" \
            r"SELECT COALESCE(MAX(BH.BanDtTm), 0) AS LastBanDtTm " \
            r"FROM neutralverse_BanHist BH WHERE BH.RedditUserId = RU.RedditUserId ) " \
            r"AND NS.T5Id = '" + subreddit_id + "' " \
            r"ORDER BY SC.FirstCommentId DESC, SC.SubmittedDtTm DESC, RTH.Points DESC"
    return get_results_dic(query)



def mark_violation_comment_restored(comment_id, is_mod_comment_restored=True):
    is_mod_comment_restored_clause = r"SET IsCommentModRestored = 1,CommentModRestoredDtTm = GETUTCDATE() "
    if not is_mod_comment_restored:
        is_mod_comment_restored_clause = r"SET IsCommentModRestored = 0 "
    query = r"UPDATE neutralverse_RuleViolationHist " + is_mod_comment_restored_clause + \
            r"FROM neutralverse_RuleViolationHist RVH " \
            r"join neutralverse_SubmissionComment SC on SC.SubmissionCommentId = RVH.SubmissionCommentId " \
            r"WHERE SC.T1Id = '" + comment_id + r"' "
    return update_rows(query)


def set_comment_last_update_dt_tm(comment):
    query = r"UPDATE neutralverse_SubmissionComment " \
            r"SET LastUpdateDtTm ='" + datetime.fromtimestamp(comment.edited).strftime("%Y-%m-%d %H:%M:%S") + "' " \
            r"WHERE SC.T1Id = '" + comment.id + r"' "
    return update_rows(query)


def get_neutral_award_given(comment_fullname):
    query = r"SELECT NAG.* FROM neutralverse_NeutralAwardGiven NAG " \
            r"where NAG.RedditFullname = '" + comment_fullname + "'"
    results = get_results_dic(query)
    return check_unique_result(results, 'comment_fullname', comment_fullname)


def get_neutral_award_given_awardboard(subreddit_id, username, is_submission=None):
    is_submission_qual = ''
    if is_submission is not None:
        if is_submission:
            is_submission_qual = 'AND NAR.IsSubmission = 1 '
        else:
            is_submission_qual = 'AND NAR.IsSubmission = 0 '
    query = r"SELECT NAG.AwardGivenDtTm AS AwardDtTm, NAG.Permalink, NAG.SubmissionTitle, NAG.SubmissionPermalink," \
            r" RU2.UserName AS Awarded " \
            r"FROM neutralverse_NeutralAwardGiven NAG " \
            r"JOIN neutralverse_NeutralverseSubreddit NS ON NS.SubredditId = NAG.SubredditId " \
            r"JOIN neutralverse_RedditUser RU ON NAG.RedditUserId = RU.RedditUserId " \
            r"JOIN neutralverse_NeutralAwardReceived NAR ON NAR.NeutralAwardGivenId = NAG.NeutralAwardGivenId " \
            r"JOIN neutralverse_RedditUser RU2 ON NAR.RedditUserId = RU2.RedditUserId " \
            r"WHERE RU.UserName = '" + username + r"' " \
            r"AND NS.T5Id = '" + subreddit_id + r"' " + is_submission_qual
    return get_results_dic(query)


def get_neutral_award_received_awardboard(subreddit_id, username, is_submission=None):
    is_submission_qual = ''
    if is_submission is not None:
        if is_submission:
            is_submission_qual = 'AND NAR.IsSubmission = 1 '
        else:
            is_submission_qual = 'AND NAR.IsSubmission = 0 '
    query = r"SELECT NAR.AwardReceivedDtTm AS AwardDtTm, NAR.Permalink, NAR.SubmissionTitle, NAR.SubmissionPermalink," \
            r" RU2.UserName AS Awarded " \
            r"FROM neutralverse_NeutralAwardReceived NAR " \
            r"JOIN neutralverse_NeutralverseSubreddit NS ON NS.SubredditId = NAR.SubredditId " \
            r"JOIN neutralverse_RedditUser RU ON NAR.RedditUserId = RU.RedditUserId " \
            r"JOIN neutralverse_NeutralAwardGiven NAG ON NAR.NeutralAwardGivenId = NAG.NeutralAwardGivenId " \
            r"JOIN neutralverse_RedditUser RU2 ON NAG.RedditUserId = RU2.RedditUserId " \
            r"WHERE RU.UserName = '" + username + r"' " \
            r"AND NS.T5Id = '" + subreddit_id + r"' " + is_submission_qual
    return get_results_dic(query)


def get_neutral_award_count(subreddit_id, username):
    query = r"SELECT COUNT(*) FROM neutralverse_NeutralAwardReceived NAR " \
            r"JOIN neutralverse_RedditUser RU ON RU.RedditUserId = NAR.RedditUserId " \
            r"JOIN neutralverse_NeutralverseSubreddit NS ON NS.SubredditId = NAR.SubredditId " \
            r"WHERE NS.T5Id = '" + subreddit_id + r" ' " \
            r"AND RU.UserName = '" + username + r" ' "
    return get_results_dic(query)[0]['']


def get_award_top_ten(subreddit_id, lookback_dt_tm=None):
    lookback_dt_tm_qual = ''
    if lookback_dt_tm is not None:
        lookback_dt_tm_qual = r"AND NAR.AwardReceivedDtTm >= '"\
                              + lookback_dt_tm.strftime("%Y-%m-%d %H:%M:%S") + r"'"
    query = r"SELECT RU.UserName, COUNT(*) AS Awards FROM neutralverse_NeutralAwardReceived NAR " \
            r"JOIN neutralverse_RedditUser RU ON RU.RedditUserId = NAR.RedditUserId " \
            r"JOIN neutralverse_NeutralverseSubreddit NS ON NS.SubredditId = NAR.SubredditId " \
            r"WHERE NS.T5Id = '" + subreddit_id + r"' " + lookback_dt_tm_qual + r" " \
            r"GROUP BY RU.UserName " \
            r"ORDER BY Awards desc  limit 10"
    return get_results_dic(query)


def get_award_count_from_same_user_to_parent(comment):
    query = r"SELECT count(*) FROM neutralverse_NeutralAwardReceived NAR " \
            r"JOIN neutralverse_NeutralAwardGiven NAG on NAR.NeutralAwardGivenId = NAG.NeutralAwardGivenId " \
            r"JOIN neutralverse_NeutralAwardGiven NAG2 on NAG2.RedditUserId = NAG.RedditUserId " \
            r"JOIN neutralverse_RedditUser RU on NAR.RedditUserId = RU.RedditUserId " \
            r"WHERE NAR.RedditFullname = '" + comment.parent().fullname + "' " \
            r"AND RU.UserName = '" + comment.author.name + "' "
    return get_results_dic(query)[0]['']


def create_neutral_award_given(comment):
    award_given_dt_tm = datetime.fromtimestamp(comment.created_utc).strftime("%Y-%m-%d %H:%M:%S")
    subreddit_id = get_subreddit_or_create(comment.subreddit)['SubredditId']
    user_id = get_user_or_create(comment.author)['RedditUserId']
    submission = comment.submission
    neutral_award_given_data = [{'SubredditId': str(subreddit_id), 'RedditUserId': user_id,
                                 'AwardGivenDtTm': award_given_dt_tm, 'Permalink': comment.permalink,
                                 'SubmissionTitle': submission.title, 'SubmissionPermalink': submission.permalink,
                                 'RedditFullname': comment.fullname}]
    insert_rows('neutralverse_NeutralAwardGiven', neutral_award_given_data)


def create_neutral_award_received(awarded_item, awarding_comment):
    award_receive_dt_tm = datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
    neutral_award_given_id = get_neutral_award_given(awarding_comment.fullname)['NeutralAwardGivenId']
    subreddit_id = get_subreddit_or_create(awarded_item.subreddit)['SubredditId']
    user_id = get_user_or_create(awarded_item.author)['RedditUserId']
    is_submission = awarded_item.fullname.startswith('t3_')
    submission = awarding_comment.submission
    neutral_award_received_data = [{'NeutralAwardGivenId': neutral_award_given_id, 'SubredditId': str(subreddit_id),
                                    'RedditUserId': user_id, 'AwardReceivedDtTm': award_receive_dt_tm,
                                    'Permalink': awarded_item.permalink, 'RedditFullname': awarded_item.fullname,
                                    'SubmissionTitle': submission.title, 'SubmissionPermalink': submission.permalink,
                                    'IsSubmission': is_submission}]
    insert_rows('neutralverse_NeutralAwardReceived', neutral_award_received_data)


def award_parent(comment):
    create_neutral_award_given(comment)
    create_neutral_award_received(comment.parent(), comment)


def pm_opt_out_change(username, opt_out=True):
    if opt_out:
        query = r"MERGE moderation_PrivateMessageBlacklist WITH (HOLDLOCK) AS PMB " \
                r"USING (SELECT RU.RedditUserId " \
                r"FROM neutralverse_RedditUser RU " \
                r"WHERE RU.UserName = '" + username + r"' " \
                r"AND NOT EXISTS(SELECT 1 " \
                r"FROM neutralverse_RedditUser RU2 " \
                r"JOIN moderation_PrivateMessageBlacklist PMB2 ON RU2.RedditUserId = PMB2.RedditUserId " \
                r"WHERE RU2.UserName = '" + username + r"' )) T " \
                r"ON T.RedditUserId = PMB.RedditUserId " \
                r"WHEN NOT MATCHED BY TARGET THEN " \
                r"INSERT (RedditUserId) VALUES(T.RedditUserId);"
    else:
        query = r"DELETE FROM moderation_PrivateMessageBlacklist " \
                r"WHERE RedditUserId IN (SELECT RU.RedditUserId from neutralverse_RedditUser RU " \
                r"WHERE RU.UserName = '" + username + r"') "
    conn = get_connection()
    cursor = conn.cursor()
    print('executing query', query)
    cursor.execute(query)
    conn.commit()
    cursor.close()
    conn.close()


def get_pm_opt_out_list():
    query = r"SELECT RU.UserName FROM moderation_PrivateMessageBlacklist PMB " \
            r"JOIN neutralverse_RedditUser RU ON RU.RedditUserId = PMB.RedditUserId "
    return get_results_dic(query)
