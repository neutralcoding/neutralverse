from Neutralbot.neutralbot.tools import reddit_tools, database_connector as db_conn


def get_total_points(reddit, violations, violations_settings, prev_ban):
    total_points = 0
    if prev_ban is not None:
        total_points = prev_ban["PointsThreshold"]

    last_comment_id = ''
    last_first_comment_id = ''
    last_rule = ''
    comment_in_chain_added = False
    for violation in violations:
        comment_id = violation["T1Id"]
        first_comment_id = violation["FirstCommentId"]
        rule = violation["Name"]
        if first_comment_id != last_first_comment_id:
            comment_in_chain_added = False

        if (comment_id == last_comment_id and violations_settings["AggregateViolationsInComment"]) \
                or (comment_id != last_comment_id
                    and (violations_settings["AggregateViolationsInChain"]
                         or not comment_in_chain_added
                         or rule != last_rule)):
            if not (violation["IsCommentModRestored"] and violation["LastUpdateDtTm"] is None):
                comment_in_chain_added = True
                # If a comment is none, it was deleted
                fullname = reddit_tools.comment_type + comment_id
                if reddit_tools.get_comment(reddit, fullname) is None:
                    total_points += violation["Points"] * violations_settings["DeleteRefundPercent"] / 100
                elif violation["IsCommentModRestored"]:
                    total_points += violation["Points"] * violations_settings["RestoreRefundPercent"] / 100
                else:
                    total_points += violation["Points"]
        last_comment_id = comment_id
        last_first_comment_id = first_comment_id
        last_rule = rule
    return total_points


def get_points_total_for_user(reddit, username, subreddit_id):
    class User:
        name = username

    user = User()
    violations_settings = db_conn.get_violations_settings(subreddit_id)
    violations = db_conn.get_rule_violations(user, subreddit_id, violations_settings["TrackDurationDays"])
    ban = db_conn.get_previous_ban(user, subreddit_id)
    total_points = get_total_points(reddit, violations, violations_settings, ban)
    return total_points
