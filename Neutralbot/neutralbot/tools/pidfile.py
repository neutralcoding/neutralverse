"""
used for logging the pid and info about a process that is supposed to
be running, so we can alert when it's not
"""
from pathlib import Path
import os
import re
import sys
import json
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

ROOT = Path("/home/ec2-user/Neutralbot/pid")

slack_token = "xoxb-22061933542-2040726184885-bCdHw71sHNv6WjICaUMflzbY"  # os.getenv('SLACK_BOT_TOKEN') or sys.argv[1]
client = WebClient(token=slack_token)


def log_pid():
    """
    all our processes look like

    python3 /home/ec2-user/Neutralbot/neutralbot/actions/violation_tracker.py

    so for convenience, we'll just auto-detect the program and store
    the resulting key in a file like

    pids/neutralbot-actions-violation_tracker.pid
    """
    filename = sys.argv[0]
    print("running python script:", filename)
    filename = filename.replace("/home/ec2-user/Neutralbot/", "")
    filename = filename.replace("/", "-")
    filename = re.sub(r"\.py$", ".pid", filename)
    print("sanitized script:", filename)
    data = {
        "pid": os.getpid(),
        "command": sys.argv[0],
    }
    with (ROOT / filename).open("w") as fp:
        json.dump(data, fp, indent=3)


def alert(title, message):
    blocks = [
        {"type": "section", "text": {"type": "mrkdwn", "text": title}},
        {"type": "section", "fields": [{"type": "mrkdwn", "text": message}]},
    ]
    client.chat_postMessage(
        channel="#coding",
        blocks=blocks,
        unfurl_links=False,
        unfurl_media=False,
        # text="test",
    )


def scan():
    """go through the pid directory and alert if there's any process
    that isn't running, or if we have an unparseable pid file which
    might be the result of an empty disk"""
    for path in ROOT.glob("*.pid"):
        print(path)
        try:
            with path.open() as fp:
                data = json.load(fp)
        except:
            alert(
                "possible bot issue",
                f"issue reading pid file {path}",
            )
            continue
        print("expecting pid", data["pid"])
        try:
            os.kill(data["pid"], 0)
        except ProcessLookupError as e:
            pid = data["pid"]
            command = data["command"]
            alert(
                "possible bot issue",
                f"pid {pid} for program {command} is not running!",
            )


if __name__ == "__main__":
    """
    if we run this standalone, it can work as an audit tool to let us
    know when things are wrong
    """
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--scan", action="store_true")
    parser.add_argument("--log_pid", action="store_true")
    args = parser.parse_args()
    print(args)
    if args.log_pid:
        log_pid()

    if args.scan:
        scan()
