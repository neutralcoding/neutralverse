import praw
import time

from datetime import datetime
from Neutralbot.neutralbot.tools import pidfile, reddit_tools


class UnstickyFeedback:
    meta_title = '[META] Monthly Feedback Thread'
    sticky_days = 1

    def __init__(self, sub_name):
        self.reddit = praw.Reddit('NeutralverseBot')
        self.reddit.validate_on_submit = True
        self.subreddit = self.reddit.subreddit(sub_name)

    def try_unsticky(self, subm):
        days = (datetime.now() - datetime.fromtimestamp(subm.created_utc)).days
        if subm.stickied and subm.title.startswith(self.meta_title) and days > self.sticky_days:
            subm.mod.sticky(state=False)
            return True
        return False

    def main(self):
        while True:
            hot_subms = self.subreddit.hot(limit=2)
            subm = next(hot_subms)
            if not self.try_unsticky(subm):
                self.try_unsticky(next(hot_subms))

            # check daily to remove thread sticky
            time.sleep(60 * 60 * 24)

def main():
    unsticky = UnstickyFeedback('neutralnews')
    unsticky.main()

if __name__ == "__main__":
    pidfile.log_pid()
    reddit_tools.smart_retry(main)
