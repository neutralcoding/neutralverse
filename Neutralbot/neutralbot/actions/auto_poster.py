from datetime import datetime
from time import mktime
from urllib.parse import urlparse
import argparse
import random
import re
import json

from apscheduler.schedulers.background import BackgroundScheduler
import feedparser
import praw
import pytz

from Neutralbot.neutralbot.tools import reddit_tools, pidfile


class AutoPoster:
    wiki_revise_action = "wikirevise"

    def __init__(self, sub_name, dryrun):
        self.dryrun = dryrun
        self.reddit = praw.Reddit('NeutralverseBot')
        self.reddit.validate_on_submit = True
        self.subreddit = self.reddit.subreddit(sub_name)
        self.scheduler = BackgroundScheduler()

    def load_rss_settings(self):
        post_limit_wiki = self.subreddit.wiki['rss-config']
        return reddit_tools.SubSettings(post_limit_wiki.content_md).values()

    def schedule_link_posts(self):
        rss_settings = self.load_rss_settings()
        rss_link_list = rss_settings["links"]
        rss_post_times_utc = [datetime.strptime(time, '%H%M') for time in rss_settings["schedule"]]
        for post_time in rss_post_times_utc:
            self.scheduler.add_job(
                self.post_random_news_link,
                args=[rss_link_list],
                trigger='cron',
                hour=post_time.hour,
                minute=post_time.minute,
                timezone=pytz.utc
            )

    def is_acceptable_url(self, url):
        """
        * domain in apnews.com or reuters.com
        * not a video link
        * not obviously sports
        * in the case of apnews, not a redirect url

        :param url: due to google being difficult about their rss feed,
            the url is actually just one of https://apnews.com/ or
            https://reuters.com/
        """
        acceptable_domains = {'apnews.com', 'reuters.com', 'www.reuters.com'}
        o = urlparse(url)
        if o.netloc not in acceptable_domains:
            print('rejecting invalid domain', o.netloc)
            return False
        if re.search('reuters.com/video', url):
            print('rejecting reuters.com/video')
            return False
        if re.search('apnews.com/sports/', url):
            print('rejecting apnews.com/sports/')
            return False
        if re.search('reuters.com/sports/', url):
            print('rejecting reuters.com/sports/')
            return False
        if re.search('-sports-', url):
            print('rejecting -sports-')
            return False
        if re.search('apnews.com/.', url) and not re.search('apnews.com/article', url):
            print('rejecting non-article apnews.com')
            return False
        return True

    def is_acceptable_time(self, publish_time):
        """
        :param time.struct_time publish_time: the time the article was
            published in UTC
        :returns bool: True if it was within the past 8 hours
        """
        publish_datetime = datetime.fromtimestamp(mktime(publish_time))
        publish_datetime = pytz.timezone('UTC').localize(publish_datetime)
        now = datetime.now(pytz.utc)
        age = now - publish_datetime
        return age.seconds < 8 * 3600

    def extract_articles(self, rss_link):
        """ extract entries that match certain criteria from an RSS

        TODO: maybe resolve the url in the case of redirects like
        https://apnews.com/406d34a4ce495812d7e96b65602177c9 which redirects to
        https://apnews.com/article/europe-tennis-french-open-sports-406d34a4ce495812d7e96b65602177c9

        :param rss_link: a url or local filename to an RSS feed
        :returns: a filtered list of RSS entries
        """
        news_feed = feedparser.parse(rss_link)
        # we don't get the full url in the feed anymore, but there's a
        # less useful key called "source" that just contains the domain
        # name, eg "https://reuters.com" that we can use to filter out
        # those edge cases where a domain we like appears in the query
        # string for an article on some other domain
        entries = [x for x in news_feed.entries
                   if self.is_acceptable_url(x.source['href'])]
        entries = [x for x in entries
                   if self.is_acceptable_time(x.published_parsed)]
        return entries

    def post_random_news_link(self, rss_link_list):
        candidate_files = [
            '/home/ec2-user/reuters.json',
            '/home/ec2-user/apnews.json'
        ]
        filename = random.choice(candidate_files)
        print('selecting article from', filename)
        with open(filename) as fp:
            data = json.load(fp)
        for attempt in range(10):
            entry = random.choice(data)
            sanitized_link = entry['url']
            title = entry['title']
            print('candidate link:', sanitized_link, title)
            if title.startswith('The Latest:'):
                print('Skipping because the title started with "The Latest:"')
                continue
            if not self.is_acceptable_url(sanitized_link):
                print('skipping because it was deemed unacceptable after following the redirect chain')
                continue
            break
        else:
            print('failed to find a good link in 10 attempts, giving up')
            return
        print('final selection:', sanitized_link)
        print('title:', title)
        if self.dryrun:
            return
        flair_id = reddit_tools.get_flair_template_id(self.subreddit, "BOT POST")
        if flair_id is not None:
            self.subreddit.submit(title, url=sanitized_link, flair_id=flair_id)
        else:
            submission = self.subreddit.submit(title, url=sanitized_link)
            submission.mod.flair('BOT POST')


    def schedule_jobs(self):
        self.scheduler.remove_all_jobs()
        self.schedule_link_posts()
        self.scheduler.start()
        self.scheduler.print_jobs()

    def main(self):
        try:
            rss_config_wiki = self.subreddit.wiki['rss-config']
            rss_config_last_revision_date = rss_config_wiki.revision_date
            self.schedule_jobs()

            for _ in self.subreddit.mod.stream.log(action=self.wiki_revise_action, skip_existing=True):
                rss_config_wiki = self.subreddit.wiki['rss-config']
                if rss_config_last_revision_date < rss_config_wiki.revision_date:
                    rss_config_last_revision_date = rss_config_wiki.revision_date
                    self.schedule_jobs()
        except Exception:
            # If there's an unhandled exception, kill all scheduled jobs
            self.scheduler.remove_all_jobs()
            raise


def other_main():
    """ sorry about the name """
    poster = AutoPoster("neutralnews", False)
    poster.main()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--subreddit', type=str, default='neutralnews',
                        help='set the subreddit to post to, defaults to neutralnews')
    parser.add_argument('--now', action='store_true',
                        help='post an article immediately and exit instead of using the scheduler')
    parser.add_argument('--dryrun', action='store_true',
                        help="go through article selection but don't actually post to reddit")
    args = parser.parse_args()
    if args.now:
        poster = AutoPoster(args.subreddit, args.dryrun)
        rss_settings = poster.load_rss_settings()
        rss_link_list = rss_settings["links"]
        poster.post_random_news_link(rss_link_list)
        return
    if args.dryrun:
        poster = AutoPoster(args.subreddit, args.dryrun)
        poster.main()
        return
    pidfile.log_pid()
    reddit_tools.smart_retry(other_main)


if __name__ == "__main__":
    main()
