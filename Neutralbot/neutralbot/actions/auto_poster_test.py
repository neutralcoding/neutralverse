from unittest.mock import MagicMock
from urllib.parse import urlparse
from datetime import datetime
import json

import pytest
from freezegun import freeze_time

from Neutralbot.neutralbot.actions.auto_poster import AutoPoster


@pytest.fixture(autouse=True)
def patch_reddit(monkeypatch):
    mock_reddit = MagicMock()
    monkeypatch.setattr('praw.Reddit', lambda x: mock_reddit)


@freeze_time('2021-06-03 15:26')
def test_apnews_time_filter():
    auto_poster = AutoPoster('neutralnews')
    entries = auto_poster.extract_articles('Neutralbot/neutralbot/actions/tests/data/apnews_rss_72h.xml')
    expected = [
        "https://apnews.com/article/johnson-and-johnson-us-supreme-court-michael-brown-business-health-e31b4a561cf43461844db28b44c4194d",
        "https://apnews.com/article/china-bird-flu-flu-health-b5862e1d9892b25fdb470abf30432289",
        "https://apnews.com/article/iran-largest-warship-catches-fire-sinks-gulf-oman-f3e8126a8603326e1abc4705c6629fcd",
        "https://apnews.com/article/only-on-ap-middle-east-europe-government-and-politics-76acafd6547fb7cc9ef03c0dd0156eab",
        "https://apnews.com/article/health-europe-germany-pollution-air-pollution-dca7bcc3ecca56925e28b3245b4f4233",
        "https://apnews.com/article/united-nations-coronavirus-vaccine-coronavirus-pandemic-business-health-21fcf0a3b03dccd3dc63720a788fca16",
        "https://apnews.com/article/jobless-claims-coronavirus-pandemic-health-pandemics-business-8a27c3e8dc30ddb81e0091aa3d9c3cb5",
        "https://apnews.com/article/asia-pacific-europe-poland-china-business-2c7d6f2e3c42f7883f54b3d70e7fae6c",
        "https://apnews.com/article/us-news-george-floyd-death-of-george-floyd-4802f492f10c83d95da9c34f496737e0",
        "https://apnews.com/article/united-nations-africa-europe-migration-government-and-politics-a199bb4b9990696a857eeb40f455cb25",
        "https://apnews.com/article/amsterdam-tests-electric-autonomous-boats-25e91a558f9066b290f7c36ad7f3391d",
        "https://apnews.com/article/europe-health-coronavirus-pandemic-migration-government-and-politics-a2d044e66d31b71851f7b6aa74a0f8cc",
        "https://apnews.com/article/serie-a-simone-inzaghi-europe-health-coronavirus-pandemic-471f47899f891e8d47d6170e38baaa3d",
        "https://apnews.com/article/donald-trump-entertainment-music-voting-rights-ahmaud-arbery-b2bc86b6f37029f157050a5c1d91a0be",
        "https://apnews.com/article/new-york-rangers-europe-scotland-soccer-health-9ff29e96d59359d828cc52304ec8ccdf",
        "https://apnews.com/article/capitol-insurrection-charges-roil-far-right-groups-1e0560dbd5572944e3435e225f8be616",
        "https://apnews.com/article/tokyo-health-coronavirus-pandemic-olympic-games-2020-tokyo-olympics-e7350e3562f420ef35f51b71b558aa8e",
        "https://apnews.com/article/europe-belarus-journalists-government-and-politics-114a2d3eeb28a67c302d0e7decbb775e",
        "https://apnews.com/article/united-nations-pandemics-coronavirus-pandemic-health-business-41f4fe61fd1a6389796b1833337a1882",
        "https://apnews.com/article/pa-state-wire-business-b6a5c9f5e6415e6a6de33da3d238827d",
        "https://apnews.com/article/business-health-coronavirus-pandemic-government-and-politics-16f4c4fcc0ebfa374c1bd78ea41a672c",
        "https://apnews.com/article/business-womens-soccer-coronavirus-pandemic-health-pandemics-d1407d57a74fd2ee63285b5f1854291f",
        "https://apnews.com/article/health-coronavirus-pandemic-business-959a1a97847ec7a1dc18d96ade834a8d",
        "https://apnews.com/article/music-entertainment-lifestyle-arts-and-entertainment-2aeedf2b9cf72c6ddab1485b7c09a786",
        "https://apnews.com/article/health-coronavirus-pandemic-business-87906dda5cc82926343efa3bee166dd7"
    ]
    urls = [x.link for x in entries]
    save = False
    if save:
        with open('expected', 'w') as fp:
            json.dump(urls, fp, indent=4)
    assert(urls == expected)


@freeze_time('2021-06-04 16:12')
def test_reuters_time_filter():
    auto_poster = AutoPoster('neutralnews')
    entries = auto_poster.extract_articles('Neutralbot/neutralbot/actions/tests/data/reuters_rss_72h.xml')
    expected = [
        "https://www.reuters.com/article/global-oil-opec-idAFL2N2NK0NL",
        "https://www.reuters.com/world/middle-east/egypt-sends-building-equipment-begin-gaza-reconstruction-state-tv-2021-06-04/",
        "https://www.reuters.com/world/china/teslas-china-orders-halved-may-information-2021-06-03/",
        "https://www.reuters.com/business/finance/climate-change-risks-will-affect-us-bank-capital-long-run-official-2021-06-02/",
        "https://www.reuters.com/world/europe/germany-persistently-broke-air-pollution-rules-eu-top-court-rules-2021-06-03/",
        "https://www.reuters.com/business/autos-transportation/ford-says-it-could-face-13-billion-new-penalties-after-court-ruling-2021-06-03/",
        "https://www.reuters.com/world/europe/eu-launches-digital-identity-wallet-driven-by-pandemic-digital-push-2021-06-03/",
        "https://www.reuters.com/world/europe/eu-cant-work-like-this-german-official-says-after-hungary-blocks-china-statement-2021-06-04/",
        "https://www.reuters.com/business/sustainable-business/bill-gates-eu-pledge-1-billion-boost-green-technology-2021-06-02/",
        "https://www.reuters.com/technology/musk-tweet-dents-bitcoin-weekly-gain-prospect-2021-06-04/",
        "https://www.reuters.com/business/sustainable-business/tesla-buy-more-than-1-bln-australian-battery-minerals-year-2021-06-02/",
        "https://www.reuters.com/world/putin-inks-law-ban-extremists-elections-amid-navalny-crackdown-2021-06-04/",
        "https://www.reuters.com/technology/walmart-give-740000-us-store-workers-free-samsung-phones-2021-06-03/",
        "https://www.reuters.com/business/ackmans-spac-nearing-deal-with-universal-music-group-wsj-2021-06-03/",
        "https://www.reuters.com/business/healthcare-pharmaceuticals/us-authorizes-lower-dose-regenerons-covid-19-antibody-therapy-2021-06-04/",
        "https://www.reuters.com/world/middle-east/israel-sees-probable-link-between-pfizer-vaccine-small-number-myocarditis-cases-2021-06-01/",
        "https://www.reuters.com/business/aerospace-defense/ryanair-says-it-may-not-take-first-737-max-until-after-summer-2021-06-03/",
        "https://www.reuters.com/world/africa/apprentice-fashion-icon-ivory-coasts-patheo-50-years-2021-06-04/",
        "https://www.reuters.com/world/middle-east/saudi-aramco-talks-with-banks-new-bond-sale-sources-2021-06-02/",
        "https://www.reuters.com/world/middle-east/turkish-delivery-firm-getir-raises-555-mln-sets-sights-us-market-2021-06-04/",
        "https://www.reuters.com/world/europe/eu-ban-belarus-overflights-midnight-diplomats-say-2021-06-04/",
        "https://www.reuters.com/business/healthcare-pharmaceuticals/fauci-calls-china-release-medical-records-wuhan-lab-workers-ft-2021-06-04/",
        "https://www.reuters.com/technology/eu-antitrust-regulators-investige-facebooks-marketplace-2021-06-04/",
        "https://www.reuters.com/world/middle-east/decision-remove-candidates-iran-election-be-reviewed-2021-06-04/",
        "https://www.reuters.com/world/india/india-cenbank-keeps-rates-record-low-virus-lashes-economy-2021-06-04/",
        "https://www.reuters.com/business/g7-finance-ministers-meet-london-broker-global-tax-deal-2021-06-03/",
        "https://www.reuters.com/world/russian-court-rejects-navalnys-attempt-quash-his-flight-risk-jail-status-2021-06-02/",
        "https://www.reuters.com/business/sustainable-business/japans-top-regional-bank-resona-deploy-92-bln-sustainable-finance-by-2030-2021-06-02/",
        "https://www.reuters.com/world/china/so-far-low-risk-human-spread-h10n3-bird-flu-2021-06-02/",
        "https://www.reuters.com/business/environment/co2-emissions-coal-shipping-stay-strong-2020-green-pressures-grow-2021-06-02/",
        "https://www.reuters.com/world/europe/germany-agrees-reform-increase-care-worker-pay-2021-06-02/",
        "https://www.reuters.com/world/china/worlds-coal-producers-now-planning-more-than-400-new-mines-research-2021-06-03/",
        "https://www.reuters.com/business/energy/brazils-vale-closes-mines-after-prosecutors-mandate-evacuation-near-xingu-dam-2021-06-04/",
        "https://www.reuters.com/article/us-italy-economy/italy-statistics-bureau-hikes-2021-growth-forecast-as-covid-clouds-clear-idUSKCN2DG0V3",
        "https://www.reuters.com/world/asia-pacific/japan-provide-12-mln-doses-coronavirus-vaccine-taiwan-2021-06-04/",
        "https://www.reuters.com/world/americas/brazil-registers-almost-100000-new-covid-19-cases-wednesday-2021-05-31/",
        "https://www.reuters.com/world/middle-east/lebanon-asks-un-explore-means-funding-special-tribunal-2021-06-04/",
        "https://www.reuters.com/business/healthcare-pharmaceuticals/who-urges-countries-follow-us-give-vaccine-doses-fill-gap-2021-06-04/",
        "https://www.reuters.com/business/environment/sri-lanka-readies-oil-spill-sunken-cargo-ship-2021-06-03/",
        "https://www.reuters.com/technology/german-competition-authority-launches-probe-into-googles-news-showcase-2021-06-04/",
        "https://www.reuters.com/world/uk/uk-regulator-says-cryptoasset-firms-not-meeting-anti-money-laundering-rules-2021-06-03/",
        "https://www.reuters.com/business/retail-consumer/beyond-meat-names-amazon-vp-cfo-2021-06-04/",
        "https://www.reuters.com/breakingviews/swiss-launch-covid-19-certificates-next-week-2021-06-04/",
        "https://www.reuters.com/world/middle-east/iran-navy-training-ship-fire-mouth-gulf-crew-evacuated-report-2021-06-01/",
        "https://www.reuters.com/lifestyle/sports/russian-player-sizikova-arrested-french-open-source-2021-06-04/",
        "https://www.reuters.com/technology/global-markets-bitcoin-2021-06-03/",
        "https://www.reuters.com/business/finance/shanghai-hold-lottery-red-packets-digital-yuan-2021-06-04/",
        "https://www.reuters.com/breakingviews/generali-fires-low-first-shot-asset-manager-2021-06-04/",
        "https://www.reuters.com/world/europe/covid-19-keeps-high-spending-us-tourists-d-day-beaches-anniversary-2021-06-04/",
        "https://www.reuters.com/world/asia-pacific/australias-victoria-posts-slight-rise-covid-19-cases-2021-06-03/",
        "https://www.reuters.com/world/europe/french-police-dismantle-migrant-camp-housing-hundreds-calais-2021-06-04/",
        "https://www.reuters.com/business/energy/russias-idle-oil-refining-capacity-seen-down-195-july-2021-06-04/"
    ]
    urls = [x.link for x in entries]
    save = False
    if save:
        with open('expected', 'w') as fp:
            json.dump(urls, fp, indent=4)
    assert(urls == expected)


def test_acceptable_urls():
    cases = [
        (False, 'https://apnews.com/406d34a4ce495812d7e96b65602177c9'),
        (False, 'https://www.reuters.com/video/watch/idRCV009SLJ'),
        (True, 'https://www.reuters.com/world/africa/south-africa-extends-nightly-curfew-limits-gatherings-covid-19-cases-surge-2021-05-30/'),
        (True, 'https://apnews.com/article/laws-government-and-politics-education-b0555801d9f3ce49618ef61f6454e824'),
        (False, 'https://apnews.com/article/premier-league-europe-soccer-sports-c9dacede6be1ef0c7ed1426572209baf'),
        (False, 'https://flipboard.com/topic/asia/german-businessman-s-dismembered-body-found-in-freezer-in-thailand-with-chainsaw/a-xTroB41dTB6eLML0qsvVrg%3Aa%3A3199720-a9169d07de%2Fapnews.com'),
    ]
    for expected, url in cases:
        auto_poster = AutoPoster('neutralnews')
        print(url, expected, auto_poster.is_acceptable_url(url))
        assert(auto_poster.is_acceptable_url(url) == expected)


def test_post_random_news():
    """ just a smoke test to see if it raises an exception """
    auto_poster = AutoPoster('neutralnews')

    auto_poster.post_random_news_link([
        'Neutralbot/neutralbot/actions/tests/data/reuters_rss.xml'
    ])
