import praw
import re

from Neutralbot.neutralbot.tools import reddit_tools, database_connector as db_conn, pidfile
from Neutralbot.neutralbot.tools.reddit_tools import reddit_base_link
from logging import getLogger

logger = getLogger(__name__)


# noinspection SpellCheckingInspection
class ViolationTracker:
    comment_type = 't1_'
    submission_type = 't3_'
    distinguish_action = 'distinguish'
    approve_comment_action = 'approvecomment'
    lock_action = 'lock'
    unlock_action = 'unlock'

    def __init__(self, sub_name):
        self.reddit = praw.Reddit('NeutralverseBot')
        self.reddit.validate_on_submit = True
        self.subreddit = self.reddit.subreddit(sub_name)

    def approve_comment_response(self, comment, log):
        if comment is None or comment.author is None:
            return
        rules_violated = db_conn.get_rules_violated(comment.id)
        if rules_violated is not None and len(rules_violated) > 0:
            db_conn.mark_violation_comment_restored(comment.id)
            if comment.edited > 0:
                db_conn.set_comment_last_update_dt_tm(comment)
                previous_ban = db_conn.get_previous_ban(comment.author, self.subreddit.id)
                ban_votes = db_conn.get_relevant_ban_votes(comment.author, previous_ban)
                ban_vote = next((vote for vote in ban_votes if vote["ActiveStatusFlag"]), None)
                if ban_vote is not None:
                    rules_joined = ', '.join(list(rule['Name'] for rule in rules_violated))
                    mod_message = 'u/' + log.mod.name + ' restored a comment that was removed for breaking rules [' \
                                  + rules_joined + ' ]: ' + reddit_base_link + comment.permalink + '.'
                    reddit_tools.add_ban_vote_comment(self.reddit, ban_vote, mod_message)
                user = comment.author
                violations_settings = db_conn.get_violations_settings(self.subreddit.id)
                violations = db_conn.get_rule_violations(user, self.subreddit.id,
                                                         violations_settings["TrackDurationDays"])
                prev_ban = db_conn.get_previous_ban(user, self.subreddit.id)
                total_points = self.get_total_points(violations, violations_settings, prev_ban)
                reddit_tools.update_points_total_wiki(self.reddit, self.subreddit.display_name, user.name, total_points)

    def load_rules(self):
        violations_wiki = self.subreddit.wiki['violations-config']
        rules = reddit_tools.SubSettings(violations_wiki.content_md).values()
        tracked_rules = rules['Rules']
        ban_types = rules['BanTypes']
        violation_settings = rules['ViolationSettings']

        db_conn.update_rules(tracked_rules, self.subreddit)
        db_conn.update_ban_types(ban_types, self.subreddit)
        db_conn.update_violations_settings(violation_settings, self.subreddit)
        return db_conn.get_rules(self.subreddit.id)

    def get_total_points(self, violations, violations_settings, prev_ban):
        total_points = 0
        if prev_ban is not None:
            total_points = prev_ban["PointsThreshold"]

        last_comment_id = ''
        last_first_comment_id = ''
        last_rule = ''
        comment_in_chain_added = False
        for violation in violations:
            comment_id = violation["T1Id"]
            first_comment_id = violation["FirstCommentId"]
            rule = violation["Name"]
            if first_comment_id != last_first_comment_id:
                comment_in_chain_added = False

            if (comment_id == last_comment_id and violations_settings["AggregateViolationsInComment"]) \
                or (comment_id != last_comment_id
                    and (violations_settings["AggregateViolationsInChain"]
                         or not comment_in_chain_added
                         or rule != last_rule)):
                if not (violation["IsCommentModRestored"] and violation["LastUpdateDtTm"] is None):
                    comment_in_chain_added = True
                    # If a comment is none, it was deleted
                    fullname = reddit_tools.comment_type + comment_id
                    if reddit_tools.get_comment(self.reddit, fullname) is None:
                        total_points += violation["Points"] * violations_settings["DeleteRefundPercent"] / 100
                    elif violation["IsCommentModRestored"]:
                        total_points += violation["Points"] * violations_settings["RestoreRefundPercent"] / 100
                    else:
                        total_points += violation["Points"]
            last_comment_id = comment_id
            last_first_comment_id = first_comment_id
            last_rule = rule
        return total_points

    def handle_rule_violation(self, mod_comment, user_comment, root_comment, rule, rules):
        violation = db_conn.get_rule_violation(user_comment, root_comment, rule["Name"])
        if violation is not None and not violation["IsCommentModRestored"]:
            return

        user = user_comment.author
        db_conn.get_user_or_create(user)
        if violation is None:
            db_conn.create_rule_violation_hist(user_comment, rule["Name"], rule["Version"], root_comment, mod_comment)
        elif violation["IsCommentModRestored"]:
            db_conn.mark_violation_comment_restored(user_comment.id, False)

        violations_settings = db_conn.get_violations_settings(self.subreddit.id)
        violations = db_conn.get_rule_violations(user, self.subreddit.id, violations_settings["TrackDurationDays"])
        prev_ban = db_conn.get_previous_ban(user, self.subreddit.id)
        ban = db_conn.get_next_ban_type(prev_ban, self.subreddit.id)
        total_points = self.get_total_points(violations, violations_settings, prev_ban)
        reddit_tools.update_points_total_wiki(self.reddit, self.subreddit.display_name, user.name, total_points)

        if total_points >= ban["PointsThreshold"]:
            ban_votes = db_conn.get_relevant_ban_votes(user, prev_ban)
            ban_vote = next((vote for vote in ban_votes if vote["ActiveStatusFlag"]), None)
            if ban_vote is not None:
                comment_link = reddit_base_link + user_comment.permalink
                mod_message = 'Rule [' + rule["Name"] + '] violation occurred after vote began: ' + comment_link + '.'
                reddit_tools.add_ban_vote_comment(self.reddit, ban_vote, mod_message)
            elif total_points >= ban["PointsThreshold"]:
                if ban['IsAutomatic']:
                    reddit_tools.ban_user(self.subreddit, user, ban, violations)
                else:
                    violations = db_conn.get_rule_violations(user, self.subreddit.id)
                    prev_ban_vote = db_conn.get_ban_vote_for_ban(prev_ban)
                    modmail_title = reddit_tools.start_ban_vote(self.subreddit, ban, prev_ban, prev_ban_vote, ban_votes,
                                                                user, violations, rules)
                    modmail = reddit_tools.lookup_mail_by_title(self.reddit, modmail_title)
                    db_conn.create_ban_vote(modmail, user, ban['BanTypeId'], ban['BanTypeVersion'])

    def distinguish_comment_response(self, comment):
        user_comment = comment.parent()
        if comment.is_root or not reddit_tools.is_comment_loadable(user_comment) or user_comment.author is None:
            return

        rules = self.load_rules()
        root_comment = reddit_tools.get_root_comment(comment)
        for rule in rules:
            for section in re.split(r'[\r\n]+', comment.body, flags=re.MULTILINE):
                if ("//" + str(rule["Name"]).lower()) in section.lower():
                    self.handle_rule_violation(comment, user_comment, root_comment, rule, rules)

    def lock_submission_response(self, submission):
        submission.comment_sort = "top"
        submission.comment_limit = 1
        comment = submission.comments.list()[0]

        if comment.author == "NeutralverseBot":
            violation_messages = reddit_tools.load_custom_messages(self.subreddit, "ViolationTracker")
            comment.edit(violation_messages["locked"] + comment.body)

    def unlock_submission_response(self, submission):
        submission.comment_sort = "top"
        submission.comment_limit = 1
        comment = submission.comments.list()[0]

        if comment.author == "NeutralverseBot":
            violation_messages = reddit_tools.load_custom_messages(self.subreddit, "ViolationTracker")
            comment.edit(violation_messages["unlocked"] + comment.body)

    def main(self):
        # from Neutralbot.neutralbot.tools import api_endpoints
        # points = api_endpoints.get_points_total_for_user(self.reddit, 'fiji-refugee', self.subreddit.id)
        while True:
            for log in self.subreddit.mod.stream.log(skip_existing=True):
                if log is None:
                    break
                if log.target_fullname is None:
                    continue
                elif str(log.target_fullname).startswith(self.comment_type):
                    comment = reddit_tools.get_comment(self.reddit, log.target_fullname)
                    if comment is None:
                        continue
                    elif log.action == self.approve_comment_action:
                        self.approve_comment_response(comment, log)
                    elif log.action == self.distinguish_action:
                        self.distinguish_comment_response(comment)
                elif str(log.target_fullname).startswith(self.submission_type):
                    submission = reddit_tools.get_submission(self.reddit, log.target_fullname)
                    if submission is None:
                        continue
                    if log.action == self.lock_action:
                        self.lock_submission_response(submission)
                    elif log.action == self.unlock_action:
                        self.unlock_submission_response(submission)


def main():
    logger.info('main starting')
    tracker = ViolationTracker('neutralnews')
    tracker.main()


if __name__ == "__main__":
    pidfile.log_pid()
    reddit_tools.smart_retry(main)
