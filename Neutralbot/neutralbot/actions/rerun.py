import praw
from Neutralbot.neutralbot.tools import reddit_tools, database_connector as db_conn


# noinspection SpellCheckingInspection
class RerunTracking:
    comment_type = 't1_'
    submission_type = 't3_'
    distinguish_action = 'distinguish'
    approve_comment_action = 'approvecomment'
    lock_action = 'lock'
    unlock_action = 'unlock'

    def __init__(self, sub_name):
        self.reddit = praw.Reddit('NeutralverseBot')
        self.reddit.validate_on_submit = True
        self.subreddit = self.reddit.subreddit(sub_name)

    def load_rules(self):
        violations_wiki = self.subreddit.wiki['violations-config']
        rules = reddit_tools.SubSettings(violations_wiki.content_md).values()
        tracked_rules = rules['Rules']
        ban_types = rules['BanTypes']
        violation_settings = rules['ViolationSettings']

        db_conn.update_rules(tracked_rules, self.subreddit)
        db_conn.update_ban_types(ban_types, self.subreddit)
        db_conn.update_violations_settings(violation_settings, self.subreddit)
        return db_conn.get_rules(self.subreddit.id)

    def get_total_points(self, violations, violations_settings, prev_ban):
        total_points = 0
        if prev_ban is not None:
            total_points = prev_ban["PointsThreshold"]

        last_comment_id = ''
        last_first_comment_id = ''
        last_rule = ''
        comment_in_chain_added = False
        for violation in violations:
            comment_id = violation["T1Id"]
            first_comment_id = violation["FirstCommentId"]
            rule = violation["Name"]
            if first_comment_id != last_first_comment_id:
                comment_in_chain_added = False

            if (comment_id == last_comment_id and violations_settings["AggregateViolationsInComment"]) \
                or (comment_id != last_comment_id
                    and (violations_settings["AggregateViolationsInChain"]
                         or not comment_in_chain_added
                         or rule != last_rule)):
                if not (violation["IsCommentModRestored"] and violation["LastUpdateDtTm"] is None):
                    comment_in_chain_added = True
                    # If a comment is none, it was deleted
                    fullname = reddit_tools.comment_type + comment_id
                    if reddit_tools.get_comment(self.reddit, fullname) is None:
                        total_points += violation["Points"] * violations_settings["DeleteRefundPercent"] / 100
                    elif violation["IsCommentModRestored"]:
                        total_points += violation["Points"] * violations_settings["RestoreRefundPercent"] / 100
                    else:
                        total_points += violation["Points"]
            last_comment_id = comment_id
            last_first_comment_id = first_comment_id
            last_rule = rule
        return total_points

    def main(self):
        # from Neutralbot.neutralbot.tools import api_endpoints
        # points = api_endpoints.get_points_total_for_user(self.reddit, 'fiji-refugee', self.subreddit.id)

        query = 'select RU.UserName from neutralverse.RedditUser RU where RU.RedditUserId > 1069'
        usernames = db_conn.get_results_dic(query)
        rules = self.load_rules()
        violations_settings = db_conn.get_violations_settings(self.subreddit.id)
        for name in usernames:
            username = name["UserName"]
            if username is None or username == '':
                continue
            user = self.reddit.redditor(username)
            violations = db_conn.get_rule_violations(user, self.subreddit.id, violations_settings["TrackDurationDays"])
            prev_ban = db_conn.get_previous_ban(user, self.subreddit.id)
            ban = db_conn.get_next_ban_type(prev_ban, self.subreddit.id)
            total_points = self.get_total_points(violations, violations_settings, prev_ban)
            try:
                reddit_tools.update_points_total_wiki(self.reddit, self.subreddit.display_name, user.name, total_points)
            except Exception as e:
                # reddit frequently 500s so the try except will keep it running even if that happens
                print(e)
            if total_points >= ban["PointsThreshold"] and total_points >= ban["PointsThreshold"]:
                next_ban = db_conn.get_next_ban_type(ban, self.subreddit.id)
                while next_ban is not None and total_points >= next_ban["PointsThreshold"] and next_ban["BanLengthDays"] < 999:
                    ban = next_ban
                    next_ban = db_conn.get_next_ban_type(ban, self.subreddit.id)
                ban_votes = db_conn.get_relevant_ban_votes(user, prev_ban)
                if ban['IsAutomatic']:
                    reddit_tools.ban_user(self.subreddit, user, ban, violations)
                else:
                    violations = db_conn.get_rule_violations(user, self.subreddit.id)
                    prev_ban_vote = db_conn.get_ban_vote_for_ban(prev_ban)
                    modmail_title = reddit_tools.start_ban_vote(self.subreddit, ban, prev_ban, prev_ban_vote, ban_votes,
                                                                user, violations, rules)
                    modmail = reddit_tools.lookup_mail_by_title(self.reddit, modmail_title)
                    db_conn.create_ban_vote(modmail, user, ban['BanTypeId'], ban['BanTypeVersion'])


if __name__ == "__main__":
    while True:
        try:
            tracker = RerunTracking('neutralnews')
            tracker.main()
        except Exception as e:
            # reddit frequently 500s so the try except will keep it running even if that happens
            print(e)