import logging
from logging.handlers import TimedRotatingFileHandler

import praw
from praw.models import Submission
from fuzzywuzzy import process
from date_guesser import guess_date
from datetime import datetime

from Neutralbot.neutralbot.tools import reddit_tools, database_connector as db_conn, pidfile


class SubmissionChecker:
    match_percent_minimum = 88
    max_age = 7

    def __init__(self, sub_name):
        self.log_file = 'submission_checker.info'
        self.formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(lineno)d - %(message)s")
        self.logger = logging.getLogger('submission_checker')
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('Initialize ' + 'submission_checker')

        file_handler = TimedRotatingFileHandler(self.log_file, when="midnight")
        file_handler.setFormatter(self.formatter)
        file_handler.setLevel(logging.DEBUG)
        self.logger.addHandler(file_handler)

        self.reddit = praw.Reddit('NeutralverseBot')
        self.reddit.validate_on_submit = True
        self.subreddit = self.reddit.subreddit(sub_name)

        self.bot_message = "\n\n*I am a bot, and this action was performed automatically. " \
                           "Please [contact the moderators of this subreddit](/message/compose/?to=/r/" + \
                           self.subreddit.display_name + ") if you have any questions or concerns.*"

    def load_post_limit_settings(self):
        post_limit_wiki = self.subreddit.wiki['postlimit']
        return reddit_tools.SubSettings(post_limit_wiki.content_md).values()

    @staticmethod
    def approve_submission(submission, message=None, remove_flair=True):
        if remove_flair:
            submission.mod.flair('')
        submission.mod.approve()
        if message is not None:
            comment = submission.reply(message)
            comment.mod.distinguish(sticky=True)
            comment.mod.lock()
        db_conn.get_submission_or_create(submission, False)

    @staticmethod
    def remove_submission(submission, message=None):
        if message is not None:
            comment = submission.reply(message)
            comment.mod.distinguish(sticky=True)
        submission.mod.flair('')
        submission.mod.remove()
        db_conn.get_submission_or_create(submission, True)

    def post_limit_exceeded(self, submission, posts_limit, period):
        if not submission.distinguished:
            if db_conn.get_submission(submission.id) is None \
                    and submission.author != 'NeutralverseBot':
                db_conn.get_user_or_create(submission.author)
                submission_cnt = db_conn.get_submission_cnt(submission.author.name, self.subreddit.id, period)
                return submission_cnt >= posts_limit
        return False

    def handle_valid_title_check(self, subm):
        try:
            if subm.link_flair_text == 'Date and Title OK':
                return True
            titles = reddit_tools.get_titles(subm.url)
            if titles is None or len(titles) == 0:
                # Prefer false negatives. If we can't determine title, assume it's correct
                return True
            value = process.extractOne(subm.title, titles)
            self.logger.info(f"extracted titles: {titles}")
            self.logger.info(f"match result: {value}")
            if value[1] > self.match_percent_minimum:
                if titles.get('og_title') and not value[2] == 'og_title':
                    self.logger.info("flairing as Updated Headline In Story")
                    subm.mod.flair('Updated Headline In Story')
                return True
            else:
                self.logger.info("flairing as MOD: Check Title")
                subm.mod.flair('MOD: Check Title')
                return False
        except (ConnectionError, TimeoutError, Exception):
            return True

    def check_article_age(self, subm):
        try:
            guess = guess_date(url=subm.url, html=reddit_tools.get_html_text(subm.url))
            if guess is None or guess.date is None:
                # Prefer false negatives. If we can't determine date, assume it's within boundary
                return True
            return abs((datetime.utcnow().date() - guess.date.date()).days) <= self.max_age
        except (ConnectionError, TimeoutError, Exception):
            return True

    def handle_flair(self, submission, submission_messages):
        """Checks comment for flairs that indicate specific action. Returns whether comment in removed"""

        flair_text = submission.link_flair_text
        if flair_text == 'Approve':
            self.approve_submission(submission, message=submission_messages["auto_sticky"])
        elif flair_text == 'BOT POST':
            self.approve_submission(submission, message=submission_messages["auto_sticky"], remove_flair=False)
        elif flair_text == 'Reject':
            self.remove_submission(submission, message=submission_messages["reject"])
        elif flair_text == 'Acceptlist':
            acceptlist_wiki = self.subreddit.wiki['acceptlist']
            acceptlist_wiki.edit(acceptlist_wiki.content_md + "\n    " + submission.domain)
            self.approve_submission(submission, message=submission_messages["auto_sticky"])
        elif flair_text == 'Rejectlist':
            rejectlist_wiki = self.subreddit.wiki['rejectlist']
            rejectlist_wiki.edit(rejectlist_wiki.content_md + "\n    " + submission.domain)
            self.remove_submission(submission, message=submission_messages["rejectlist"])
        elif flair_text == 'Date and Title OK':
            self.handle_list_checks(submission, submission_messages)
        elif flair_text == 'Updated Headline In Story':
            self.approve_submission(submission, message=submission_messages["auto_sticky"], remove_flair=False)
        elif flair_text == 'Title Fail':
            self.remove_submission(submission, message=submission_messages["title_fail"] + self.bot_message)
        elif flair_text == 'Date Fail':
            self.remove_submission(submission, message=submission_messages["date_fail"] + self.bot_message)

    def handle_list_checks(self, submission, submission_messages):
        acceptlist = self.subreddit.wiki['acceptlist'].content_md.replace(" ", "").splitlines()
        rejectlist = self.subreddit.wiki['rejectlist'].content_md.replace(" ", "").splitlines()
        domain = submission.domain.lower()

        in_acceptlist = domain in (dom.lower() for dom in acceptlist)
        in_rejectlist = domain in (dom.lower() for dom in rejectlist)
        if in_acceptlist and in_rejectlist:
            submission.mod.flair('MOD: Domain on both lists')
        elif in_acceptlist:
            if submission.author == 'NeutralverseBot' or self.handle_valid_title_check(submission):
                self.approve_submission(submission, message=submission_messages["auto_sticky"])
        elif in_rejectlist:
            self.remove_submission(submission, message=submission_messages["rejectlist"])
        elif submission.link_flair_text != 'MOD: Domain on neither list':
            submission.mod.flair('MOD: Domain on neither list')

    def main(self):
        while True:
            self.logger.info('Begin modqueue stream')
            for subm in self.subreddit.mod.stream.modqueue(pause_after=5):
                if subm is None:
                    self.logger.info('Restarting queue')
                    break

                self.logger.debug('Begin evaluate queue item: ' + subm.permalink)
                if not type(subm) is Submission or subm.approved:
                    # Ignore anything in the mod queue that has already been approved by a mod previously:
                    continue

                submission_messages = reddit_tools.load_custom_messages(self.subreddit, "SubmissionChecker")
                post_limits = self.load_post_limit_settings()
                if subm.permalink in subm.url:
                    # Don't count meta posts to post limit
                    # Text posts are mod only, but they want to have them stay removed to allow other mods to edit
                    # Bot posts are excepted since its messages are previously approved
                    if subm.author.name == 'NeutralverseBot' or subm.author.name == 'AutoModerator':
                        subm.mod.approve()
                        subm.mod.flair('META')
                elif self.post_limit_exceeded(subm, post_limits["posts"], post_limits["days"]):
                    next_date = db_conn.get_next_post_date(subm.author.name, self.subreddit.id, post_limits["days"])
                    message = post_limits["message"] + "\n\nNext submission can be made on " + \
                        next_date.strftime("%Y%m%d %H:%M:%S %Z")
                    self.remove_submission(subm, message=message)
                elif subm.link_flair_text is not None and subm.link_flair_text != '':
                    self.handle_flair(subm, submission_messages)
                else:
                    self.handle_list_checks(subm, submission_messages)


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info('Submission checker starting')

    checker = SubmissionChecker('neutralnews')
    checker.main()


if __name__ == "__main__":
    pidfile.log_pid()
    reddit_tools.smart_retry(main)
