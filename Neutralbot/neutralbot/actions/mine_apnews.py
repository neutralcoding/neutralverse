from bs4 import BeautifulSoup
import re
import json
from scrapingbee import ScrapingBeeClient
from scrapingbee_settings import SCRAPINGBEE_KEY


def check(url):
    if not re.match(r"https://apnews.com/article/", url):
        print('fail', url)
        return False
    return True


def main():
    if True:
        client = ScrapingBeeClient(api_key=SCRAPINGBEE_KEY)
        response = client.get("https://apnews.com/", params={"premium_proxy": "True"})
        with open('apnews.raw', 'wb') as fp:
            fp.write(response.content)
        soup = BeautifulSoup(response.content, features="lxml")
    else:
        with open('apnews.raw', 'rb') as fp:
            content = fp.read()
        soup = BeautifulSoup(content, features="lxml")

    seen = set()
    results = []
    for tag in soup.find_all("a"):
        try:
            url = tag["href"]
        except Exception:
            continue
        title = tag.text.strip()
        if not title:
            continue
        if not check(url):
            continue
        if url in seen:
            continue
        seen.add(url)
        results.append(
            {
                "url": url,
                "title": title,
            }
        )

    with open("apnews.json", "w") as fp:
        json.dump(results, fp, indent=4)


if __name__ == "__main__":
    main()
