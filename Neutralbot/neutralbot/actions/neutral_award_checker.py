import logging
from logging.handlers import TimedRotatingFileHandler

import praw
import re

from praw.models.util import stream_generator
from praw.reddit import Comment

from Neutralbot.neutralbot.tools import reddit_tools, database_connector


class ReportChecker:
    award_symbols = ['&#9878;',  # Libra scales
                     '!merit'
                     ]
    min_comment_length = 0
    awards_regex_str = '((' + ')|('.join(award_symbols).replace('!', r'\!') + '))'
    block_quote_regex = '^(?!>)(?!    ).*?(' + awards_regex_str + ')'

    def __init__(self, sub_name):
        self.log_file = 'neutral_award_checker.info'
        self.formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(lineno)d - %(message)s")
        self.logger = logging.getLogger('neutral_award_checker')
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('Initialize ' + 'neutral_award_checker')
        file_handler = TimedRotatingFileHandler(self.log_file, when="midnight")
        file_handler.setFormatter(self.formatter)
        file_handler.setLevel(logging.DEBUG)
        self.logger.addHandler(file_handler)

        self.reddit = praw.Reddit('NeutralverseBot')
        self.reddit.validate_on_submit = True
        self.subreddit = self.reddit.subreddit(sub_name)

    def check_for_award(self, awarding_comment):
        custom_user_messages_wiki = self.subreddit.wiki['custom-user-messages']
        messages = reddit_tools.SubSettings(custom_user_messages_wiki.content_md).values()["NeutralAwardChecker"]
        lines = re.split(r'[\r\n]+', awarding_comment.body, re.MULTILINE)
        if any(re.match(self.block_quote_regex, line, re.MULTILINE | re.IGNORECASE) for line in lines):
            if awarding_comment.author == awarding_comment.parent().author:
                reddit_tools.message_redditor(self.subreddit, awarding_comment.author,
                                              messages['self_award']
                                              + '\n\n' + awarding_comment.permalink)
                return False
            if awarding_comment.parent().author == 'NeutralverseBot':
                reddit_tools.message_redditor(self.subreddit, awarding_comment.author, messages['award_neutralversebot']
                                              + '\n\n' + awarding_comment.permalink)
                return False
            if len(awarding_comment.body) < self.min_comment_length:
                reddit_tools.message_redditor(self.subreddit, awarding_comment.author, messages['short_award_reason']
                                              + '\n\n' + awarding_comment.permalink)
                return False
        elif any(symbol in awarding_comment.body for symbol in self.award_symbols):
            reddit_tools.message_redditor(self.subreddit, awarding_comment.author, messages['award_quoted']
                                          + '\n\n' + awarding_comment.permalink)
            return False
        else:
            return False
        return True

    def main(self):
        while True:
            for comment in self.subreddit.stream.comments(skip_existing=True, pause_after=-1):
                self.logger.info('Begin comment stream')
                if comment is None:
                    break
                elif type(comment) == Comment and \
                        comment.author is not None and comment.author.name != 'NeutralverseBot':
                    self.logger.debug('Checking for merit: ' + str(comment.id))
                    parent = comment.parent()
                    comment_award_given_data = database_connector.get_neutral_award_given(comment.fullname)
                    if comment_award_given_data is not None \
                            and 0 < database_connector.get_award_count_from_same_user_to_parent(comment) \
                            and not self.check_for_award(comment):
                        continue

                    database_connector.award_parent(comment)
                    reddit_tools.update_internal_awardboards(self.subreddit, comment)

                    parent_author = parent.author
                    comment_award_count = database_connector.get_neutral_award_count(self.subreddit.id,
                                                                                     parent_author.name)
                    self.subreddit.flair.set(parent_author, ":merit_award: " + str(comment_award_count),
                                             css_class='award')

                    message = "Confirmed: 1 merit awarded to /u/" + parent_author.name + " ([" + \
                              str(comment_award_count) + " &#9878;](/r/neutralnews/wiki/user/" + parent_author.name + \
                              ")).\n\n^[Merit Explained](https://www.reddit.com/r/neutralnews/wiki/guidelines#wiki_awards)" \
                              " ^| ^[Neutralboards](https://www.reddit.com/r/neutralnews/wiki/neutralboards)"
                    bot_comment = comment.reply(message)
                    bot_comment.mod.distinguish(how='yes', sticky=False)


if __name__ == "__main__":
    while True:
        try:
            logging.basicConfig(level=logging.INFO)
            logging.info('Neutral Award checker starting')
            checker = ReportChecker('neutralnews')
            checker.main()
        except Exception as e:
            # reddit frequently 500s so the try except will keep it running even if that happens
            logging.getLogger('neutral_award_checker').exception(e)
