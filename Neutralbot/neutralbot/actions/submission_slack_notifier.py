import logging
from logging.handlers import TimedRotatingFileHandler
from datetime import datetime

import praw
from praw.models import Submission
from slack_sdk.errors import SlackApiError
from Neutralbot.neutralbot.tools import database_connector as db_conn, reddit_tools
from Neutralbot.neutralbot.tools.reddit_tools import reddit_base_link
from Neutralbot.neutralbot.tools.discord_tools import discord_say


class SubmissionSlackNotifier:
    def __init__(self, sub_name):
        self.log_file = 'submission_slack_notifier.info'
        self.formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(lineno)d - %(message)s")
        self.logger = logging.getLogger('submission_slack_notifier')
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('Initialize ' + 'submission_slack_notifier')

        file_handler = TimedRotatingFileHandler(self.log_file, when="midnight")
        file_handler.setFormatter(self.formatter)
        file_handler.setLevel(logging.DEBUG)
        self.logger.addHandler(file_handler)

        self.reddit = praw.Reddit('NeutralverseBot')
        self.reddit.validate_on_submit = True
        self.subreddit = self.reddit.subreddit(sub_name)

    def say_np_submission_received(self, subm):
        subm_date = datetime.fromtimestamp(subm.created_utc).strftime("%b %d %Y %I:%M:%S %p")
        blocks = [{
            "type": "section",
            "text": {"type": "mrkdwn",
                     "text": "New submission:\n*<" + reddit_base_link + subm.permalink + "|" + subm.title + ">*"}
        },
            {
                "type": "section",
                "fields": [
                    {
                        "type": "mrkdwn",
                        "text": "*Author:*\n<" + reddit_base_link + "/user/" + subm.author.name
                                + "|u/" + subm.author.name + ">"
                    },
                    {
                        "type": "mrkdwn",
                        "text": "*When:*\n" + subm_date
                    }
                ]}]
        try:
            print('sending to slack.')
            response = reddit_tools.client.chat_postMessage(
                channel="#np_submissions",
                blocks=blocks,
                unfurl_links=False,
                unfurl_media=False,
                text='test'
            )
            permalink = reddit_tools.client.chat_getPermalink(
                channel=response.data["channel"], message_ts=response.data["ts"]
            )["permalink"]
            print('sending to discord?')
            discord_say('np-submissions', f'hi there is a new post from {subm.author.name}')
            return permalink
        except SlackApiError as sae:
            assert sae.response["error"]

    def main(self):
        while True:
            # reddit_tools.client.chat_postMessage(
            #     channel="#np_submissions",
            #     text="https://giphy.com/gifs/gilmoregirls-netflix-gilmore-girls-3ohA31QLTJilArEfvi",
            #     unfurl_links=True,
            #     unfurl_media=True,
            #     thread_ts="1655916582.717169"
            # )
            self.logger.info('Begin modqueue stream')
            for subm in self.subreddit.mod.stream.modqueue(pause_after=5):
                if subm is None:
                    self.logger.info('Restarting queue')
                    break

                self.logger.debug('Begin evaluate queue item: ' + subm.permalink)
                if not type(subm) is Submission or subm.approved:
                    # Ignore anything in the mod queue that has already been approved by a mod previously:
                    continue
                subm_data = db_conn.get_submission(subm.id)
                if subm_data is None:
                    db_conn.create_submission(subm)
                    link = self.say_np_submission_received(subm)
                    message = '[Please follow discussion on slack thread.](' + link + ')'
                    comment = subm.reply(message)
                    comment.mod.remove(spam=False)


if __name__ == "__main__":
    while True:
        try:
            logging.basicConfig(level=logging.INFO)
            logging.info('Submission slack notifier starting')

            checker = SubmissionSlackNotifier('neutralpolitics')
            checker.main()
        except Exception as e:
            # reddit frequently 500s so the try except will keep it running even if that happens
            logging.getLogger('submission_slack_notifier').exception(e)
