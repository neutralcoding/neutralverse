from distutils.util import strtobool

import praw
import re

from Neutralbot.neutralbot.tools import reddit_tools, database_connector, pidfile


class InboxChecker:
    ban_vote_title_prefix = 'ban vote for '
    opt_out_change_title = 'opt_out_change'

    ban_vote_user_token = 'User: '
    ban_vote_subreddit_token = 'Subreddit: '
    ban_vote_user_regex = re.compile('.*' + ban_vote_user_token + '(.*?) ')
    ban_vote_subreddit_regex = re.compile('.*' + ban_vote_subreddit_token + '(.*?)$')

    accept_ban_result = 'Accept ban.'
    cancel_ban_result = 'Cancel ban.'

    bad_syntax_message = 'Unable to parse message, please check your formatting and try again.'
    unsubscribe_successful_message = 'Successfully unsubscribed to PMs from this bot. If this was done in error or if you ' \
                                     'would like to resubscribe, click [here]' \
                                     r"(http://www.reddit.com/message/compose/?to=NeutralverseBot&subject=opt_out_change" \
                                     r"&message=opt_out:False)"
    subscribe_successful_message = 'Successfully subscribed to PMs from this bot. If this was done in error or if you ' \
                                   'would like to unsubscribe, click [here]' \
                                   r"(http://www.reddit.com/message/compose/?to=NeutralverseBot&subject=opt_out_change" \
                                   r"&message=opt_out:True)"

    comment_type = "t1_"

    def __init__(self):
        self.reddit = praw.Reddit('NeutralverseBot')
        self.reddit.validate_on_submit = True

    def main(self):
        for message in self.reddit.inbox.stream():
            if message.subject.lower().startswith(self.ban_vote_title_prefix):
                user_name = self.ban_vote_user_regex.search(message.subject).group(1)
                subreddit_name = self.ban_vote_subreddit_regex.search(message.subject).group(1)

                subreddit = self.reddit.subreddit(subreddit_name)
                ban_result = message.body
                user = self.reddit.redditor(user_name)
                ban_vote = database_connector.get_active_ban_vote(user.name, subreddit.id)

                if ban_result == self.accept_ban_result:
                    ban_type_data = database_connector.get_ban_type(ban_vote['BanTypeId'], ban_vote['BanTypeVersion'])
                    reddit_tools.ban_user(subreddit, user, ban_type_data, None, ban_vote)
                elif ban_result == self.cancel_ban_result:
                    database_connector.set_ban_vote_active_ind(ban_vote['BanVoteId'], False)
                reddit_tools.add_ban_vote_comment(self.reddit, ban_vote, 'Ban action [' + ban_result + '] confirmed')
            elif message.subject.lower() == self.opt_out_change_title:
                try:
                    opt_out = strtobool(message.body.split(':')[1])
                    database_connector.pm_opt_out_change(message.author.name, opt_out)
                    if opt_out:
                        message.reply(self.unsubscribe_successful_message)
                    else:
                        message.reply(self.subscribe_successful_message)
                except ValueError:
                    message.reply(self.bad_syntax_message)
            elif message.was_comment and message.parent().fullname.startswith(self.comment_type):
                message.report('Please review')
            message.mark_read()


def main():
    checker = InboxChecker()
    checker.main()


if __name__ == "__main__":
    pidfile.log_pid()
    reddit_tools.smart_retry(main)
