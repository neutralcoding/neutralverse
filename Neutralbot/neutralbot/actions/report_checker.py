import praw
from praw.models import Submission, Comment
from fuzzywuzzy import process
from Neutralbot.neutralbot.tools import reddit_tools, pidfile


class ReportChecker:
    title_rule = 'Article title does not match submission title'
    match_percent_minimum = 88

    def __init__(self, sub_name):
        self.reddit = praw.Reddit('NeutralverseBot')
        self.reddit.validate_on_submit = True
        self.subreddit = self.reddit.subreddit(sub_name)

    @staticmethod
    def comment_reply(user_comment, reply):
        """Comment on the reply, leaving a distinguished reply"""

        bot_comment = user_comment.reply(reply)
        bot_comment.mod.distinguish(how='yes', sticky=False)
        return bot_comment

    def handle_mod_reports(self, reported_comment):
        """Go through and check the modlog reports, filter mod reports and then
         if not None pass to comment_reply()"""

        try:
            if reported_comment.mod_reports is None \
                    or len(reported_comment.mod_reports) == 0:
                return
            report = reported_comment.mod_reports[0][0]
            if report is not None and report.startswith('| '):
                rule_title_regex = report[len("| "):report.index(":") + 1]
                rule_text = reddit_tools.get_rule_from_toolbox(self.subreddit, rule_title_regex)
                mod_text = "^((mod:" + reported_comment.mod_reports[0][1] + ")^)"
                reply = rule_text + "\n\n" + mod_text
                self.comment_reply(reported_comment, reply)
                reported_comment.mod.remove(spam=False)
                return
        except IndexError:
            pass

    def handle_submission_reports(self, reported):
        reports = []
        reports.extend(reported.user_reports + reported.mod_reports)
        for report in reports:
            if report is not None and report[0] == self.title_rule:
                return self.title_reported_check(reported)

    def title_reported_check(self, reported):
        try:
            titles = reddit_tools.get_titles(reported.url)
            if titles is None or len(titles) == 0 \
                    or (reported.link_flair_text is not None
                        and reported.link_flair_text == 'Updated Headline In Story'):
                # If the bot can't check the title, alert the mods to check
                reported.mod.flair('MOD: Check Title')
                return
            value = process.extractOne(reported.title, titles)
            if value[1] > self.match_percent_minimum \
                    and titles.get('og_title') and not value[2] == 'og_title':
                reported.mod.flair('Updated Headline In Story')
                reported.mod.approve()
            else:
                reported.mod.flair('MOD: Check Title')
        except (ConnectionError, TimeoutError, Exception):
            reported.mod.flair('MOD: Check Title')

    @staticmethod
    def handle_distinguished_reports(reported_comment):
        """If the reported comment is distinguished, approve it"""

        if reported_comment.distinguished:
            reported_comment.mod.approve()

    def main(self):
        while True:
            # Pause after to occasionally check for mod reports on already reported comments
            for report in self.subreddit.mod.stream.reports(pause_after=5):
                if report is None:
                    break
                if type(report) is Comment:
                    self.handle_distinguished_reports(report)
                    self.handle_mod_reports(report)
                elif type(report) is Submission:
                    self.handle_submission_reports(report)


def main():
    checker = ReportChecker('neutralnews')
    checker.main()


if __name__ == "__main__":
    pidfile.log_pid()
    reddit_tools.smart_retry(main)
