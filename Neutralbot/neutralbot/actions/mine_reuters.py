from bs4 import BeautifulSoup
import re
import json
from scrapingbee import ScrapingBeeClient
from scrapingbee_settings import SCRAPINGBEE_KEY


def check(url):
    if not re.match(r"/", url):
        return False
    if not re.search(r"\d\d\d\d-\d\d-\d\d/$", url):
        return False
    if re.match(r"/(podcasts|sports|markets|breakingviews|lifestyle)", url):
        return False
    return True


def main():
    client = ScrapingBeeClient(api_key=SCRAPINGBEE_KEY)
    response = client.get("https://www.reuters.com/", params={"premium_proxy": "True"})
    soup = BeautifulSoup(response.content, features="lxml")

    seen = set()
    results = []
    for tag in soup.find_all("a"):
        url = tag["href"]
        title = tag.text
        if not title:
            continue
        if not check(url):
            continue
        if url in seen:
            continue
        seen.add(url)
        results.append(
            {
                "url": f"https://www.reuters.com{url}",
                "title": title,
            }
        )

    with open("reuters.json", "w") as fp:
        json.dump(results, fp, indent=4)


if __name__ == "__main__":
    main()
