import os
import re
import sys

import requests
import yaml
import json

from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from urllib.parse import unquote
from datetime import datetime, timezone, timedelta
from collections import namedtuple
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
from bs4 import BeautifulSoup
from praw.exceptions import ClientException

from Neutralbot.neutralbot.tools import database_connector

reddit_base_link = r'https://www.reddit.com'
modmail_ban_title = "You've been temporarily banned from participating in r/neutralnews"
comment_type = 't1_'
submission_type = 't3_'

retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["HEAD", "GET", "PUT", "DELETE", "OPTIONS", "TRACE"],
    backoff_factor=1
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)
max_ban_length = 999  # reddit max before permaban

slack_token = os.getenv('SLACK_BOT_TOKEN') or sys.argv[1]
client = WebClient(token=slack_token)


def is_comment_loadable(comment):
    # This indicates api calls will return no data
    # because the comment in on a deleted thread or quarantined sub
    try:
        comment.is_root
        return True
    except ClientException:
        return False


def get_root_comment(thread_comment):
    thread_parent = thread_comment
    while not thread_parent.is_root:
        thread_parent = thread_parent.parent()
    return thread_parent


def get_flair_template_id(subreddit, flair_text):
    for flair in subreddit.flair.link_templates:
        if flair["text"] == flair_text:
            return flair["id"]
    return None


def say_ban_notice(ban_type_data, user, subreddit):
    title = str(ban_type_data['BanLengthDays']) + ' day ban notice'
    if ban_type_data['BanLengthDays'] == 0:
        title = 'Ban warning notice'
    elif ban_type_data['BanLengthDays'] == 999:
        title = 'Permaban notice'
    blocks = [{
        "type": "section",
        "text": {"type": "mrkdwn",
                 "text": title}
    },
        {
            "type": "section",
            "fields": [
                {
                    "type": "mrkdwn",
                    "text": "*User:*\n<" + reddit_base_link + "/user/" + user.name + "|u/" + user.name + ">"
                }
            ]}]
    channel = ''
    if subreddit.display_name.lower() == "neutralnews":
        channel = "#nn_general"
    elif subreddit.display_name.lower() == "neutralpolitics":
        channel = "#np_general"
    try:
        client.chat_postMessage(
            channel=channel,
            blocks=blocks,
            unfurl_links=False,
            unfurl_media=False,
            text='test'
        )
    except SlackApiError as sae:
        assert sae.response["error"]


def ban_user(subreddit, user, ban_type_data, violations, ban_vote_data=None):
    if ban_type_data['BanLengthDays'] >= max_ban_length:
        ban_message = load_custom_messages(subreddit, "General")["ban_message"]
        subreddit.banned.add(user.name, ban_message=ban_message)
    elif ban_type_data['BanLengthDays'] == 0:
        ban_warning_title = load_custom_messages(subreddit, "General")["ban_warning_title"]
        ban_warning_message = load_custom_messages(subreddit, "General")["ban_warning_message"]
        user.message(ban_warning_title, ban_warning_message, subreddit.display_name)
        # subreddit.modmail.create(ban_warning_title, ban_warning_message, user)
    else:
        ban_message = load_custom_messages(subreddit, "General")["ban_message"]
        subreddit.banned.add(user.name, duration=ban_type_data['BanLengthDays'], ban_message=ban_message)
    if ban_vote_data is not None:
        database_connector.create_ban_hist(ban_type_data['BanTypeId'], ban_type_data['Version'],
                                           ban_vote_data['BanVoteId'], user.name)
        database_connector.set_ban_vote_active_ind(ban_vote_data['BanVoteId'], False)
    else:
        database_connector.create_ban_hist(ban_type_data['BanTypeId'], ban_type_data['Version'], None, user.name)
        say_ban_notice(ban_type_data, user, subreddit)
        violation_message = 'Rule violating comments in the tracking window:\n'
        for violation in violations:
            violation_message += '\n* [' + violation["Name"] + ']: ' + reddit_base_link + violation["Permalink"]
        if ban_type_data['BanLengthDays'] == 0:
            message = 'Hi fellow mods,\n\n/u/' + user.name + ' has reached '\
                          + str(ban_type_data["PointsThreshold"]) + ' points and has received a warning.\n'
            ban_title = 'Ban warning notice for User: ' + user.name + ' in Subreddit: ' + subreddit.display_name
            subreddit.message(ban_title, message + violation_message)
        else:
            modmail = lookup_modmail(subreddit, modmail_ban_title, user.name)
            message = user.name + ' has reached ' + str(ban_type_data["PointsThreshold"]) + ' points'
            modmail.reply(message + violation_message, internal=True)


def start_ban_vote(subreddit, ban, previous_ban, previous_ban_vote, relevant_ban_votes, user, violations, rules):
    mod_message = 'Proposed ' + str(ban["BanLengthDays"]) + ' day ban of ' + user.name + '.\n\n'
    if ban["BanLengthDays"] == 0:
        mod_message = 'Proposed warning of ' + user.name + '.\n\n'
    elif ban["BanLengthDays"] > max_ban_length:
        mod_message = 'Proposed permanent ban of ' + user.name + '.\n\n'
    if previous_ban is None:
        mod_message += 'No previous ban is still being tracked.'
    else:
        mod_message += 'Since the user has a [previous ban](' + \
                       reddit_base_link + previous_ban_vote["ModmailPermalink"] + \
                       '), a more severe ban is being suggested.'

    violation_message = 'Rule violating comments in the tracking window:\n\n'
    for violation in violations:
        violation_message += '[' + violation["Name"] + ']: ' + reddit_base_link + violation["Permalink"] + '\n\n'
    previous_ban_votes_message = ''
    if 0 < len(relevant_ban_votes):
        previous_ban_votes_message = '\n\nBelow is a list of previous ban votes that have been held that did not lead' \
                                     ' to a ban.\n\n'
    for ban_vote in relevant_ban_votes:
        previous_ban_votes_message += '[Link to ban vote](' + reddit_base_link + ban_vote["ModmailPermalink"] + ')'

    mod_message_title = 'Ban Vote for User: ' + user.name + ' in Subreddit: ' + subreddit.display_name
    ban_reply_link = r"http://www.reddit.com/message/compose/?to=NeutralverseBot&subject=" \
                     + mod_message_title + r"&message="
    ban_choice_message = 'Please reply to this modmail with your votes\n\n***\n\n[Click here to execute ban](' + \
                         ban_reply_link + 'Accept ban.)\n\n&nbsp;\n\n[Click here to cancel ban](' + \
                         ban_reply_link + 'Cancel ban.)'
    mod_message += '\n\n&nbsp;\n\n' + violation_message + '\n\n&nbsp;\n\n' + previous_ban_votes_message \
                   + '\n\n&nbsp;\n\n' + ban_choice_message
    subreddit.message(mod_message_title, mod_message)
    return mod_message_title


def add_ban_vote_comment(reddit, last_ban_vote, mod_message):
    message = reddit.inbox.message(last_ban_vote["T3Id"])
    message.reply(mod_message)


def lookup_mail_by_title(reddit, mail_title):
    inbox = reddit.inbox
    inbox.message_sort = 'new'
    for message in inbox.sent(limit=10):
        if message.subject == mail_title:
            return message
    return None


def lookup_modmail(subreddit, mail_title, recipient):
    modmail = subreddit.modmail
    modmail.message_sort = 'new'
    for message in modmail.conversations():
        if message.subject == mail_title and message.participant is not None and message.participant.name == recipient:
            return message
    return None


def update_points_total_wiki(reddit, sub_name, username, points):
    try:
        neutralmod = reddit.subreddit('NeutralMods')
        wiki = neutralmod.wiki[sub_name + "_points_summary"]
        lines = wiki.content_md.replace(" ", "").splitlines()
        for i in range(len(lines)):
            values = list(filter(None, lines[i].split('|')))
            if values[0] == username:
                lines[i] = '| ' + username + ' | ' + str(points) + ' |'
                return wiki.edit('\n'.join(lines))
        return wiki.edit(wiki.content_md + '\n| ' + username + ' | ' + str(points)) + ' |'
    except Exception as e:
        # something keeps reading as null???
        print(e)


def update_user_awardboard(subreddit, username):
    def content_builder(header, award_action, award_data):
        content = '###' + header + '\n\n'
        if award_action == 'receive':
            content += '/u/' + username + ' has received ' + str(len(award_data)) + ' awards:\n\n'
            content += '| Date | Submission | Award Comment | Awarded By |\n' \
                       '| :------: | :------: | :------: | :------: |\n'
        elif award_action == 'give':
            content += '/u/' + username + ' has given ' + str(len(award_data)) + ' awards:\n\n'
            content += '| Date | Submission | Award Comment | Awarded To |\n' \
                       '| :------: | :------: | :------: | :------: |\n'
        for award in award_data:
            submission = '[' + award["SubmissionTitle"] + '](' + award["SubmissionPermalink"] + ')'
            comment = '[' + 'Link' + '](' + award['Permalink'] + ')'
            content += '| ' + award['AwardDtTm'].strftime("%Y%m%d %I:%M:%S %p") \
                       + ' | ' + submission \
                       + ' | ' + comment \
                       + ' | /u/' + award['Awarded'] + ' |\n'
        return content

    comment_award_received_data = \
        database_connector.get_neutral_award_received_awardboard(subreddit.id, username, False)
    comment_award_given_data = database_connector.get_neutral_award_given_awardboard(subreddit.id, username, False)
    submission_award_received_data = \
        database_connector.get_neutral_award_received_awardboard(subreddit.id, username, True)
    submission_award_given_data = database_connector.get_neutral_award_given_awardboard(subreddit.id, username, True)

    userboard_content = '#Award History for u/' + username + '\n\n'

    userboard_content += '##Comment Awards\n\n'
    userboard_content += content_builder('Comment Awards Received', 'receive', comment_award_received_data)
    userboard_content += content_builder('Comment Awards Given', 'give', comment_award_given_data)

    userboard_content += '##Submission Awards\n\n'
    userboard_content += content_builder('Submission Awards Received', 'receive', submission_award_received_data)
    userboard_content += content_builder('Submission Awards Given', 'give', submission_award_given_data)

    subreddit.wiki.create('user/' + username, userboard_content)


def update_award_leaderboard(subreddit):
    def content_builder(header, board):
        content = '##' + header + '\n\n| Rank | Username | Awards |\n| :------: | :------: | :------: |\n'
        rank = 0
        for row in board:
            rank = rank + 1
            user_link = '[' + row['UserName'] + '](' + subreddit.url + 'wiki/user/' + row['UserName'] + ')'
            content += '| ' + str(rank) + ' | ' + user_link + ' | ' + str(row['Awards']) + ' |\n'
        content += '||As of ' + datetime.now(timezone.utc).strftime("%Y%m%d %I:%M:%S %p") + ' UTC||\n\n'
        return content

    today = datetime.now(timezone.utc).date()
    start_of_week = today - timedelta(days=today.weekday())
    start_of_month = today.replace(day=1)
    start_of_year = today.replace(day=1, month=1)

    daily_board = database_connector.get_award_top_ten(subreddit.id, today)
    weekly_board = database_connector.get_award_top_ten(subreddit.id, start_of_week)
    monthly_board = database_connector.get_award_top_ten(subreddit.id, start_of_month)
    yearly_board = database_connector.get_award_top_ten(subreddit.id, start_of_year)
    all_time_board = database_connector.get_award_top_ten(subreddit.id)

    leaderboard_content = content_builder('Daily', daily_board)
    leaderboard_content += content_builder('Weekly', weekly_board)
    leaderboard_content += content_builder('Monthly', monthly_board)
    leaderboard_content += content_builder('Yearly', yearly_board)
    leaderboard_content += content_builder('All Time', all_time_board)

    subreddit.wiki.create('Neutralboards', leaderboard_content)


def update_internal_awardboards(subreddit, awarding_comment):
    update_award_leaderboard(subreddit)
    update_user_awardboard(subreddit, awarding_comment.author.name)
    update_user_awardboard(subreddit, awarding_comment.parent().author.name)


def message_redditor(subreddit, redditor, message):
    opt_out_list = database_connector.get_pm_opt_out_list()
    if opt_out_list is not None \
            and len(opt_out_list) > 0 \
            and not any(user['UserName'] == redditor.name for user in opt_out_list):
        opt_out_message = 'Opt out of any future private messages from this bot [here] ' \
                          r"(http://www.reddit.com/message/compose/?to=NeutralverseBot&subject=opt_out_change" \
                          r"&message=opt_out:True)"
        bot_message = "\n\n*I am a bot, and this action was performed automatically. " + opt_out_message + \
                      "Please [contact the moderators of this subreddit](/message/compose/?to=/r/" + \
                      subreddit.display_name + ") if you have any questions or concerns.*"
        redditor.message(redditor, message + bot_message)


def get_comment(reddit, fullname):
    comment_id = fullname[len(comment_type):]
    comment = reddit.comment(id=comment_id)
    if not is_comment_loadable(comment):
        return None
    return comment


def get_submission(reddit, fullname):
    submission_id = fullname[len(submission_type):]
    submission = reddit.submission(id=submission_id)
    return submission


def get_rule_from_toolbox(subreddit, rule_title_regex):
    toolbox_wiki = subreddit.wiki['toolbox']
    toolbox_json = json.loads(toolbox_wiki.content_md)
    for reason in toolbox_json['removalReasons']['reasons']:
        if re.compile(rule_title_regex).search(reason['title']):
            return unquote(reason['text'])
    return None


def get_html_text(url):
    headers = \
        {
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "Sec-Fetch-Site": "same-origin",
            "Sec-Fetch-Mode": "navigate",
            "Sec-Fetch-User": "?1",
            "Sec-Fetch-Dest": "document",
            "Referer": "https://www.google.com/",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.9"
        }
    return http.get(url, timeout=5, headers=headers).text


def get_titles(url):
    """Fetch the contents of url and try to extract the page's og_title and title."""

    if not url or not url.startswith(('http://', 'https://')):
        return None
    try:
        html = get_html_text(url)
        return extract_titles(html)
    except Exception:
        return None


def get_title(url):
    """Fetch the contents of url and try to extract the page's title.
    Will pull the og_title if available before pulling the current title"""
    titles = get_titles(url)
    if titles is not None and len(titles) > 0:
        if "og_title" in titles:
            return titles["og_title"]
        else:
            return titles["title"]
    return None


def extract_titles(data):
    """Try to extract the page title from a string of HTML.
    An og:title meta tag and a <title> tag are grabbed if found. If using <title>,
    also attempts to trim off the site's name from the end.
    """

    bs = BeautifulSoup(data)
    if not bs or not bs.html.head:
        return
    head_soup = bs.html.head

    titles = dict()

    # try to find an og:title meta tag to use
    og_title = (head_soup.find("meta", attrs={"property": "og:title"}) or
                head_soup.find("meta", attrs={"name": "og:title"}))
    if og_title and og_title.get("content") is not None:
        titles["og_title"] = cleanup_title(og_title.get("content"))

    if head_soup.title and head_soup.title.string:
        titles["title"] = cleanup_title(head_soup.title.string)

    return titles


def cleanup_title(title):
    """Cleanup title from html. Attempts to convert html title to user visible title"""

    # remove end part that's likely to be the site's name
    # looks for last delimiter char between spaces in strings
    # delimiters: |, -, emdash, endash,
    #             left- and right-pointing double angle quotation marks
    reverse_title = title[::-1]
    to_trim = re.search(u'\s[\u00ab\u00bb\u2013\u2014|-]\s',
                        reverse_title,
                        flags=re.UNICODE)

    # only trim if it won't take off over half the title
    if to_trim and to_trim.end() < len(title) / 2:
        title = title[:-(to_trim.end())]
    if title is not None:
        # get rid of extraneous whitespace in the title
        title = re.sub(r'\s+', ' ', title, flags=re.UNICODE)
        return title.strip()
    return None


# used in YamlParts.__init__()
YamlPart = namedtuple("YamlPart", ["yaml", "values"])


class SubSettings(object):
    """A subreddit's collection of Settings."""

    def __init__(self, yaml_text=""):
        """Create a collection of Rules from YAML documents."""
        self.rules = {}
        self.rules.update(YamlParts(yaml_text).parts[0].values)

    def values(self):
        return self.rules


class YamlParts(object):
    def __init__(self, yaml_text=""):
        """Create a collection of parts from YAML documents."""

        self.parts = []
        if not yaml_text:
            return

        # We want to maintain the original YAML source sections, so we need
        # to manually split up the YAML by the document delimiter (line
        # starting with "---") and then try to load each section to see if
        # it's valid
        yaml_sections = [section.strip("\r\n")
                         for section in re.split("^---", yaml_text, flags=re.MULTILINE)]

        for section_num, section in enumerate(yaml_sections, 1):
            try:
                parsed = yaml.safe_load(section)
            except Exception as e:
                raise ValueError(
                    "YAML parsing error in section %s: %s" % (section_num, e))

            # only keep the section if the parsed result is a dict (otherwise
            # it's generally just a comment)
            if isinstance(parsed, dict):
                self.parts.append(YamlPart(yaml=section, values=parsed))

    def __iter__(self):
        """Iterate over the rules in the collection."""
        for part in self.parts:
            yield part

    def __len__(self):
        return len(self.parts)


def load_custom_messages(subreddit, key):
    return SubSettings(subreddit.wiki['custom-user-messages'].content_md).values()[key]
