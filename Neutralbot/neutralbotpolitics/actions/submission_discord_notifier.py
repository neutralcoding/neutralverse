import os
import sys
import logging
from logging.handlers import TimedRotatingFileHandler
from datetime import datetime

import asyncpraw
from asyncpraw.models import Submission

import discord
from discord.ext import commands
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

from Neutralbot.neutralbot.tools import database_connector as db_conn

reddit_base_link = r'https://www.reddit.com'
TOKEN = os.getenv('DISCORD_BOT_TOKEN') or sys.argv[6]
channel_id = int(os.getenv('DISCORD_NP_SUBMISSIONS_ID') or sys.argv[7])
bot = commands.Bot(command_prefix='!')

slack_token = os.getenv('SLACK_BOT_TOKEN') or sys.argv[1]
slack_client = WebClient(token=slack_token)


class SubmissionDiscordNotifier:
    def __init__(self, sub_name, channel):
        self.sub_name = sub_name
        self.channel = channel
        self.log_file = 'submission_discord_notifier.info'
        self.formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(lineno)d - %(message)s")
        self.logger = logging.getLogger('submission_discord_notifier')
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('Initialize ' + 'submission_discord_notifier')

        file_handler = TimedRotatingFileHandler(self.log_file, when="midnight")
        file_handler.setFormatter(self.formatter)
        file_handler.setLevel(logging.DEBUG)
        self.logger.addHandler(file_handler)

        self.reddit = asyncpraw.Reddit('NeutralverseBot')

    async def main(self):
        while True:
            subreddit = await self.reddit.subreddit(self.sub_name)
            async for subm in subreddit.mod.modqueue():
                if subm is None:
                    self.logger.info('Restarting queue')
                    break

                if not type(subm) is Submission or subm.approved:
                    # Ignore anything in the mod queue that has already been approved by a mod previously:
                    continue
                subm_data = db_conn.get_submission(subm.id)

                if subm_data is None:
                    db_conn.create_submission(subm)
                    link = await self.say_np_submission_received(subm)
                    message = '[Please follow discussion on discord thread.](' + link + ')'
                    comment = await subm.reply(message)
                    await comment.mod.remove(spam=False)
                    await self.say_np_submission_received_slack(subm)

    async def say_np_submission_received(self, subm):
        subm_date = datetime.fromtimestamp(subm.created_utc).strftime("%b %d %Y %I:%M:%S %p")
        author = "[u/" + subm.author.name + "](" + reddit_base_link + "/user/" + subm.author.name + ")"

        embed = discord.Embed(
            title="New submission:",
            description="**[" + subm.title + "](" + reddit_base_link + subm.permalink + ")**"
        )

        # Add fields (optional)
        embed.add_field(name="Author:", value=author, inline=True)
        embed.add_field(name="When", value=subm_date, inline=True)

        # Send the embed
        response = await self.channel.send(embed=embed)
        return response.jump_url

    async def say_np_submission_received_slack(self, subm):
        subm_date = datetime.fromtimestamp(subm.created_utc).strftime("%b %d %Y %I:%M:%S %p")
        blocks = [{
            "type": "section",
            "text": {"type": "mrkdwn",
                     "text": "New submission:\n*<" + reddit_base_link + subm.permalink + "|" + subm.title + ">*"}
        },
            {
                "type": "section",
                "fields": [
                    {
                        "type": "mrkdwn",
                        "text": "*Author:*\n<" + reddit_base_link + "/user/" + subm.author.name
                                + "|u/" + subm.author.name + ">"
                    },
                    {
                        "type": "mrkdwn",
                        "text": "*When:*\n" + subm_date
                    }
                ]}]
        try:
            response = slack_client.chat_postMessage(
                channel="#np_submissions",
                blocks=blocks,
                unfurl_links=False,
                unfurl_media=False,
                text='test'
            )
            permalink = slack_client.chat_getPermalink(
                channel=response.data["channel"], message_ts=response.data["ts"]
            )["permalink"]
            message = '[Please follow discussion on slack thread.](' + permalink + ')'
            comment = await subm.reply(message)
            await comment.mod.remove(spam=False)
        except SlackApiError as sae:
            assert sae.response["error"]


@bot.event
async def on_ready():
    channel = bot.get_channel(channel_id)
    while True:
        try:
            logging.basicConfig(level=logging.INFO)
            logging.info('Submission notifier starting on channel ' + channel.name)

            checker = SubmissionDiscordNotifier('neutralpolitics', channel)
            await checker.main()
        except Exception as e:
            # reddit frequently 500s so the try except will keep it running even if that happens
            logging.getLogger('submission__notifier').exception(e)


bot.run(TOKEN)
