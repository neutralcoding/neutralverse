from django.contrib import admin

from .models import Submission, Reddituser, Subreddit, Banhist, Bantype, Bantypehist, Banvote, Ruletype, Ruletypehist, Ruleviolationhist, Submissioncomment, Violationssettings


@admin.register(Submission)
class SubmissionAdmin(admin.ModelAdmin):
    list_display = ('submissionid', 'subreddit', 'reddituser', 't3id', 'permalink', 'isremoved', 'submitteddttm')


@admin.register(Reddituser)
class ReddituserAdmin(admin.ModelAdmin):
    list_display = ['reddituserid', 'username', 't2id', 't5id', 'hassystemaccess', 'issystemuser']


@admin.register(Subreddit)
class SubredditAdmin(admin.ModelAdmin):
    list_display = ['subreddit', 'name', 't5id']

@admin.register(Banhist)
class BanhistAdmin(admin.ModelAdmin):
    list_display = [
        'banhistid', 
        'bantype', 
        'reddituser', 
        'banvoteid', 
        'bandttm', 
        'bantypeversion'
    ]


@admin.register(Bantype)
class BantypeAdmin(admin.ModelAdmin):
    list_display = [
        'bantypeid', 
        'subreddit', 
        'banlengthdays', 
        'priorityorder', 
        'pointsthreshold', 
        'isautomatic', 
        'version'
    ]

@admin.register(Bantypehist)
class BantypehistAdmin(admin.ModelAdmin):
    list_display = [
        'bantype',
        'version',
        'subreddit',
        'banlengthdays',
        'priorityorder',
        'pointsthreshold',
        'isautomatic'
    ]

@admin.register(Banvote)
class BanvoteAdmin(admin.ModelAdmin):
    list_display = [
        'banvoteid',
        'subreddit',
        'reddituser',
        'modmailpermalink',
        't3id',
        'activestatusflag',
        'startdttm',
        'bantype',
        'bantypeversion'
    ]



@admin.register(Ruletype)
class RuletypeAdmin(admin.ModelAdmin):
    list_display = [
        'ruletypeid',
        'subreddit',
        'name',
        'points',
        'version'
    ]


@admin.register(Ruletypehist)
class RuletypehistAdmin(admin.ModelAdmin):
    list_display = [
        'ruletype',
        'version',
        'subreddit',
        'name',
        'points'
    ]


@admin.register(Ruleviolationhist)
class RuleviolationhistAdmin(admin.ModelAdmin):
    list_display = [
        'ruleviolationhistid',
        'ruletype',
        'submissioncomment',
        'moduser',
        'iscommentmodrestored',
        'commentmodrestoreddttm',
        'modt1id',
        'ruletypeversion',
    ]



@admin.register(Submissioncomment)
class SubmissioncommentAdmin(admin.ModelAdmin):
    list_display = [
        'submissioncommentid',
        'subreddit',
        'reddituser',
        't1id',
        'firstcommentid',
        'permalink',
        'submitteddttm',
        'lastupdatedttm'
    ]


@admin.register(Violationssettings)
class ViolationssettingsAdmin(admin.ModelAdmin):
    list_display = [
        'violationssettingsid',
        'subreddit',
        'trackdurationdays',
        'restorerefundpercent',
        'deleterefundpercent',
        'aggregateviolationsincomment',
        'aggregateviolationsinchain'
    ]


