# Generated by Django 5.1.5 on 2025-01-16 03:07

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bantype',
            fields=[
                ('bantypeid', models.AutoField(db_column='BanTypeId', primary_key=True, serialize=False)),
                ('banlengthdays', models.IntegerField(db_column='BanLengthDays')),
                ('priorityorder', models.IntegerField(db_column='PriorityOrder')),
                ('pointsthreshold', models.IntegerField(db_column='PointsThreshold')),
                ('isautomatic', models.IntegerField(db_column='IsAutomatic')),
                ('version', models.IntegerField(db_column='Version')),
            ],
            options={
                'db_table': 'moderation_BanType',
            },
        ),
        migrations.CreateModel(
            name='Reddituser',
            fields=[
                ('reddituserid', models.AutoField(db_column='RedditUserId', primary_key=True, serialize=False)),
                ('username', models.CharField(db_column='UserName', max_length=20)),
                ('t2id', models.CharField(blank=True, db_column='T2Id', max_length=20, null=True)),
                ('t5id', models.CharField(blank=True, db_column='T5Id', max_length=20, null=True)),
                ('hassystemaccess', models.BooleanField(db_column='HasSystemAccess')),
                ('issystemuser', models.BooleanField(db_column='IsSystemUser')),
            ],
            options={
                'db_table': 'neutralverse_RedditUser',
            },
        ),
        migrations.CreateModel(
            name='Ruletype',
            fields=[
                ('ruletypeid', models.AutoField(db_column='RuleTypeId', primary_key=True, serialize=False)),
                ('name', models.CharField(db_column='Name', max_length=20)),
                ('points', models.IntegerField(db_column='Points')),
                ('version', models.IntegerField(db_column='Version')),
            ],
            options={
                'db_table': 'moderation_RuleType',
            },
        ),
        migrations.CreateModel(
            name='Subreddit',
            fields=[
                ('subreddit', models.AutoField(db_column='SubredditId', primary_key=True, serialize=False)),
                ('name', models.CharField(db_column='Name', max_length=20)),
                ('t5id', models.CharField(blank=True, db_column='T5Id', max_length=20, null=True)),
                ('isenabled', models.BooleanField(db_column='IsEnabled')),
                ('createdby', models.IntegerField(db_column='CreatedBy')),
                ('createddate', models.CharField(db_column='CreatedDate', max_length=23)),
                ('updatedby', models.IntegerField(db_column='UpdatedBy')),
                ('updateddate', models.CharField(db_column='UpdatedDate', max_length=23)),
            ],
            options={
                'db_table': 'neutralverse_NeutralverseSubreddit',
            },
        ),
        migrations.CreateModel(
            name='Privatemessageblacklist',
            fields=[
                ('pid', models.AutoField(db_column='PrivatemessageBlacklistId', primary_key=True, serialize=False)),
                ('reddituser', models.ForeignKey(db_column='RedditUserId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.reddituser')),
            ],
            options={
                'db_table': 'moderation_PrivateMessageBlacklist',
            },
        ),
        migrations.CreateModel(
            name='Neutralawardgiven',
            fields=[
                ('neutralawardgivenid', models.AutoField(db_column='NeutralAwardGivenId', primary_key=True, serialize=False)),
                ('awardgivendttm', models.DateTimeField(db_column='AwardGivenDtTm')),
                ('permalink', models.CharField(db_column='Permalink', max_length=255)),
                ('redditfullname', models.CharField(blank=True, db_column='RedditFullname', max_length=20, null=True)),
                ('submissiontitle', models.CharField(db_column='SubmissionTitle', max_length=255)),
                ('submissionpermalink', models.CharField(db_column='SubmissionPermalink', max_length=255)),
                ('reddituser', models.ForeignKey(db_column='RedditUserId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.reddituser')),
                ('subreddit', models.ForeignKey(db_column='SubredditId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.subreddit')),
            ],
            options={
                'db_table': 'neutralverse_NeutralAwardGiven',
            },
        ),
        migrations.CreateModel(
            name='Banhist',
            fields=[
                ('banhistid', models.AutoField(db_column='BanHistId', primary_key=True, serialize=False)),
                ('banvoteid', models.IntegerField(blank=True, db_column='BanVoteId', null=True)),
                ('bandttm', models.DateTimeField(db_column='BanDtTm')),
                ('bantypeversion', models.IntegerField(db_column='BanTypeVersion')),
                ('bantype', models.ForeignKey(db_column='BanTypeId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.bantype')),
                ('reddituser', models.ForeignKey(db_column='RedditUserId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.reddituser')),
            ],
            options={
                'db_table': 'neutralverse_BanHist',
            },
        ),
        migrations.CreateModel(
            name='Submissioncomment',
            fields=[
                ('submissioncommentid', models.AutoField(db_column='SubmissionCommentId', primary_key=True, serialize=False)),
                ('t1id', models.CharField(blank=True, db_column='T1Id', max_length=20, null=True)),
                ('firstcommentid', models.CharField(db_column='FirstCommentId', max_length=20)),
                ('permalink', models.CharField(blank=True, db_column='Permalink', max_length=255, null=True)),
                ('submitteddttm', models.DateTimeField(db_column='SubmittedDtTm')),
                ('lastupdatedttm', models.DateTimeField(blank=True, db_column='LastUpdateDtTm', null=True)),
                ('reddituser', models.ForeignKey(db_column='RedditUserId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.reddituser')),
                ('subreddit', models.ForeignKey(db_column='SubredditId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.subreddit')),
            ],
            options={
                'db_table': 'neutralverse_SubmissionComment',
            },
        ),
        migrations.CreateModel(
            name='Ruleviolationhist',
            fields=[
                ('ruleviolationhistid', models.AutoField(db_column='RuleViolationHistId', primary_key=True, serialize=False)),
                ('iscommentmodrestored', models.BooleanField(db_column='IsCommentModRestored')),
                ('commentmodrestoreddttm', models.DateTimeField(blank=True, db_column='CommentModRestoredDtTm', null=True)),
                ('modt1id', models.CharField(blank=True, db_column='ModT1Id', max_length=20, null=True)),
                ('ruletypeversion', models.IntegerField(db_column='RuleTypeVersion')),
                ('moduser', models.ForeignKey(db_column='ModUserId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.reddituser')),
                ('ruletype', models.ForeignKey(db_column='RuleTypeId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.ruletype')),
                ('submissioncomment', models.ForeignKey(db_column='SubmissionCommentId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.submissioncomment')),
            ],
            options={
                'db_table': 'neutralverse_RuleViolationHist',
            },
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('submissionid', models.AutoField(db_column='SubmissionId', primary_key=True, serialize=False)),
                ('t3id', models.CharField(blank=True, db_column='T3Id', max_length=20, null=True)),
                ('permalink', models.CharField(db_column='Permalink', max_length=255)),
                ('isremoved', models.BooleanField(db_column='IsRemoved')),
                ('submitteddttm', models.DateTimeField(db_column='SubmittedDtTm')),
                ('reddituser', models.ForeignKey(db_column='RedditUserId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.reddituser')),
                ('subreddit', models.ForeignKey(db_column='SubredditId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.subreddit')),
            ],
            options={
                'db_table': 'neutralverse_Submission',
            },
        ),
        migrations.CreateModel(
            name='Ruletypehist',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('version', models.IntegerField(db_column='Version')),
                ('name', models.CharField(db_column='Name', max_length=20)),
                ('points', models.IntegerField(db_column='Points')),
                ('ruletype', models.ForeignKey(db_column='RuleTypeId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.ruletype')),
                ('subreddit', models.ForeignKey(db_column='SubredditId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.subreddit')),
            ],
            options={
                'db_table': 'moderation_RuleTypeHist',
            },
        ),
        migrations.AddField(
            model_name='ruletype',
            name='subreddit',
            field=models.ForeignKey(db_column='SubredditId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.subreddit'),
        ),
        migrations.CreateModel(
            name='Neutralawardreceived',
            fields=[
                ('neutralawardreceivedid', models.AutoField(db_column='NeutralAwardReceivedId', primary_key=True, serialize=False)),
                ('awardreceiveddttm', models.DateTimeField(db_column='AwardReceivedDtTm')),
                ('permalink', models.CharField(db_column='Permalink', max_length=255)),
                ('redditfullname', models.CharField(blank=True, db_column='RedditFullname', max_length=20, null=True)),
                ('issubmission', models.BooleanField(db_column='IsSubmission')),
                ('submissiontitle', models.CharField(db_column='SubmissionTitle', max_length=255)),
                ('submissionpermalink', models.CharField(db_column='SubmissionPermalink', max_length=255)),
                ('neutralawardgiven', models.ForeignKey(db_column='NeutralAwardGivenId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.neutralawardgiven')),
                ('reddituser', models.ForeignKey(db_column='RedditUserId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.reddituser')),
                ('subreddit', models.ForeignKey(db_column='SubredditId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.subreddit')),
            ],
            options={
                'db_table': 'neutralverse_NeutralAwardReceived',
            },
        ),
        migrations.CreateModel(
            name='Banvote',
            fields=[
                ('banvoteid', models.AutoField(db_column='BanVoteId', primary_key=True, serialize=False)),
                ('modmailpermalink', models.CharField(db_column='ModmailPermalink', max_length=255)),
                ('t3id', models.CharField(blank=True, db_column='T3Id', max_length=20, null=True)),
                ('activestatusflag', models.IntegerField(db_column='ActiveStatusFlag')),
                ('startdttm', models.DateTimeField(db_column='StartDtTm')),
                ('bantypeversion', models.IntegerField(db_column='BanTypeVersion')),
                ('bantype', models.ForeignKey(db_column='BanTypeId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.bantype')),
                ('reddituser', models.ForeignKey(db_column='RedditUserId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.reddituser')),
                ('subreddit', models.ForeignKey(db_column='SubredditId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.subreddit')),
            ],
            options={
                'db_table': 'neutralverse_BanVote',
            },
        ),
        migrations.CreateModel(
            name='Bantypehist',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('version', models.IntegerField(db_column='Version')),
                ('banlengthdays', models.IntegerField(db_column='BanLengthDays')),
                ('priorityorder', models.IntegerField(db_column='PriorityOrder')),
                ('pointsthreshold', models.IntegerField(db_column='PointsThreshold')),
                ('isautomatic', models.BooleanField(db_column='IsAutomatic')),
                ('bantype', models.ForeignKey(db_column='BanTypeId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.bantype')),
                ('subreddit', models.ForeignKey(db_column='SubredditId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.subreddit')),
            ],
            options={
                'db_table': 'moderation_BanTypeHist',
            },
        ),
        migrations.AddField(
            model_name='bantype',
            name='subreddit',
            field=models.ForeignKey(db_column='SubredditId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.subreddit'),
        ),
        migrations.CreateModel(
            name='Violationssettings',
            fields=[
                ('violationssettingsid', models.AutoField(db_column='ViolationsSettingsId', primary_key=True, serialize=False)),
                ('trackdurationdays', models.IntegerField(db_column='TrackDurationDays')),
                ('restorerefundpercent', models.IntegerField(db_column='RestoreRefundPercent')),
                ('deleterefundpercent', models.IntegerField(db_column='DeleteRefundPercent')),
                ('aggregateviolationsincomment', models.BooleanField(db_column='AggregateViolationsInComment')),
                ('aggregateviolationsinchain', models.BooleanField(db_column='AggregateViolationsInChain')),
                ('subreddit', models.ForeignKey(db_column='SubredditId', on_delete=django.db.models.deletion.DO_NOTHING, to='main.subreddit')),
            ],
            options={
                'db_table': 'moderation_ViolationsSettings',
            },
        ),
    ]
