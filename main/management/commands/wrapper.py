"""
the idea is it will wrap one of the existing scripts up, letting us call them like

./dmanage.sh wrapper --bot neutralbot --script auto_poster
"""
import importlib

from django.core.management import BaseCommand
import sys

from Neutralbot.neutralbot.tools import reddit_tools


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--bot', type=str, help="either neutralbot or neutralbotpolitics")
        parser.add_argument('--script', type=str, help="one of the scripts, like violation_tracker or submission_checker")

    def handle(self, *args, bot=None, script=None, **kwargs):
        module_name = f"Neutralbot.{bot}.actions.{script}"
        module = importlib.import_module(module_name)
        old_argv = sys.argv
        sys.argv = [f"{script}.py"]
        reddit_tools.smart_retry(module.main)
        sys.argv = old_argv
