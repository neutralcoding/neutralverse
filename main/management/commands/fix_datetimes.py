import datetime

from django.core.management import BaseCommand

from main.models import Submission
from django.db import connection


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        with open('bad_dates.txt') as fp:
            for line in fp:
                line = line.strip()
                parts = line.split('|')
                pk = parts[0]
                date = parts[-1]
                parsed = datetime.datetime.strptime(date, '%Y%m%d %I:%M:%S %p')
                print(pk, date, parsed)
                submission = Submission.objects.get(pk=pk)
                submission.submitteddttm = parsed
                submission.save()



