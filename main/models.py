from django.db import models
from django.utils import timezone


class Banhist(models.Model):
    """ tracks all user bans, except for manually banned users like bots
    """
    banhistid = models.AutoField(db_column='BanHistId', primary_key=True)
    bantype = models.ForeignKey(
        'Bantype', db_column='BanTypeId', on_delete=models.DO_NOTHING)
    reddituser = models.ForeignKey(
        'Reddituser', db_column='RedditUserId', on_delete=models.DO_NOTHING)
    banvoteid = models.IntegerField(
        db_column='BanVoteId', blank=True, null=True)
    bandttm = models.DateTimeField(db_column='BanDtTm')
    bantypeversion = models.IntegerField(db_column='BanTypeVersion')

    class Meta:
        db_table = 'neutralverse_BanHist'


class Bantype(models.Model):
    """ tracks ban escalation levels?  by subreddit and then duration,
    and whether they can be given automatically """
    bantypeid = models.AutoField(db_column='BanTypeId', primary_key=True)
    subreddit = models.ForeignKey(
        'Subreddit', db_column='SubredditId', on_delete=models.DO_NOTHING)
    banlengthdays = models.IntegerField(db_column='BanLengthDays')
    priorityorder = models.IntegerField(db_column='PriorityOrder')
    pointsthreshold = models.IntegerField(db_column='PointsThreshold')
    isautomatic = models.IntegerField(db_column='IsAutomatic')
    version = models.IntegerField(db_column='Version', default=1, null=True, blank=True)

    def __str__(self):
        return f'[Bantype {self.pk}/sr={self.subreddit.name}/days={self.banlengthdays}/auto={self.isautomatic}]'  # noqa

    class Meta:
        db_table = 'moderation_BanType'


class Bantypehist(models.Model):
    """ populated by a SQL trigger whenever Bantype is modified """
    bantype = models.ForeignKey(
        'Bantype', db_column='BanTypeId', on_delete=models.DO_NOTHING)
    version = models.IntegerField(db_column='Version')
    subreddit = models.ForeignKey(
        'Subreddit', db_column='SubredditId', on_delete=models.DO_NOTHING)
    banlengthdays = models.IntegerField(db_column='BanLengthDays')
    priorityorder = models.IntegerField(db_column='PriorityOrder')
    pointsthreshold = models.IntegerField(db_column='PointsThreshold')
    isautomatic = models.BooleanField(db_column='IsAutomatic')

    class Meta:
        db_table = 'moderation_BanTypeHist'


class Banvote(models.Model):
    """ TODO not quite sure """
    banvoteid = models.AutoField(db_column='BanVoteId', primary_key=True)
    subreddit = models.ForeignKey(
        'Subreddit', db_column='SubredditId', on_delete=models.DO_NOTHING)
    reddituser = models.ForeignKey(
        'Reddituser', db_column='RedditUserId', on_delete=models.DO_NOTHING)
    modmailpermalink = models.CharField(
        db_column='ModmailPermalink', max_length=255)
    t3id = models.CharField(
        db_column='T3Id', max_length=20, blank=True, null=True)
    activestatusflag = models.IntegerField(db_column='ActiveStatusFlag')
    startdttm = models.DateTimeField(db_column='StartDtTm')
    bantype = models.ForeignKey(
        'Bantype', db_column='BanTypeId', on_delete=models.DO_NOTHING)
    bantypeversion = models.IntegerField(db_column='BanTypeVersion')

    class Meta:
        db_table = 'neutralverse_BanVote'


class Neutralawardgiven(models.Model):
    """ presumably tracks "!merit" comments """
    neutralawardgivenid = models.AutoField(
        db_column='NeutralAwardGivenId', primary_key=True)
    subreddit = models.ForeignKey(
        'Subreddit', db_column='SubredditId', on_delete=models.DO_NOTHING)
    reddituser = models.ForeignKey(
        'Reddituser', db_column='RedditUserId', on_delete=models.DO_NOTHING)
    awardgivendttm = models.DateTimeField(db_column='AwardGivenDtTm')
    permalink = models.CharField(db_column='Permalink', max_length=255)
    redditfullname = models.CharField(
        db_column='RedditFullname', max_length=20, blank=True, null=True)
    submissiontitle = models.CharField(
        db_column='SubmissionTitle', max_length=255)
    submissionpermalink = models.CharField(
        db_column='SubmissionPermalink', max_length=255)

    class Meta:
        db_table = 'neutralverse_NeutralAwardGiven'


class Neutralawardreceived(models.Model):
    """ presumably tracks "!merit" comments """
    neutralawardreceivedid = models.AutoField(
        db_column='NeutralAwardReceivedId', primary_key=True)
    neutralawardgiven = models.ForeignKey(
        'Neutralawardgiven', db_column='NeutralAwardGivenId',
        on_delete=models.DO_NOTHING)
    subreddit = models.ForeignKey(
        'Subreddit', db_column='SubredditId', on_delete=models.DO_NOTHING)
    reddituser = models.ForeignKey(
        'Reddituser', db_column='RedditUserId', on_delete=models.DO_NOTHING)
    awardreceiveddttm = models.DateTimeField(db_column='AwardReceivedDtTm')
    permalink = models.CharField(db_column='Permalink', max_length=255)
    redditfullname = models.CharField(
        db_column='RedditFullname', max_length=20, blank=True, null=True)
    issubmission = models.BooleanField(db_column='IsSubmission')
    submissiontitle = models.CharField(
        db_column='SubmissionTitle', max_length=255)
    submissionpermalink = models.CharField(
        db_column='SubmissionPermalink', max_length=255)

    class Meta:
        db_table = 'neutralverse_NeutralAwardReceived'


class Subreddit(models.Model):
    """ one of the subreddits in the neutralverse """
    subreddit = models.AutoField(db_column='SubredditId', primary_key=True)
    name = models.CharField(db_column='Name', max_length=20)
    t5id = models.CharField(
        db_column='T5Id', max_length=20, blank=True, null=True)
    isenabled = models.BooleanField(db_column='IsEnabled', blank=True, null=True)
    createdby = models.IntegerField(db_column='CreatedBy', blank=True, null=True)
    createddate = models.CharField(db_column='CreatedDate', max_length=23, blank=True, null=True)
    updatedby = models.IntegerField(db_column='UpdatedBy', blank=True, null=True)
    updateddate = models.CharField(db_column='UpdatedDate', max_length=23, blank=True, null=True)

    def __str__(self):
        return f'[Subreddit {self.pk}/{self.name}]'

    class Meta:
        db_table = 'neutralverse_NeutralverseSubreddit'


class Privatemessageblacklist(models.Model):
    """ TODO this schema is incorrect, figure out the actual primary key """
    pid = models.AutoField(
        db_column='PrivatemessageBlacklistId', primary_key=True)
    reddituser = models.ForeignKey(
        'Reddituser', db_column='RedditUserId', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'moderation_PrivateMessageBlacklist'


class Reddituser(models.Model):
    """ represents a regular reddit user, and is also used to link mod
    actions in Ruleviolationhist """
    reddituserid = models.AutoField(db_column='RedditUserId', primary_key=True)
    username = models.CharField(db_column='UserName', max_length=20)
    t2id = models.CharField(
        db_column='T2Id', max_length=20, blank=True, null=True)
    t5id = models.CharField(
        db_column='T5Id', max_length=20, blank=True, null=True)
    hassystemaccess = models.BooleanField(db_column='HasSystemAccess')
    issystemuser = models.BooleanField(db_column='IsSystemUser')

    def __str__(self):
        return f'[Reddituser {self.pk}/{self.username}]'

    class Meta:
        db_table = 'neutralverse_RedditUser'


class Ruletype(models.Model):
    """ contains the different kinds of rule violations (comment r1-4,
    submission r1-5, misc, abuse, referenced from Ruleviolationhist. also
    tracks the points associated with violations for bans.
    """
    ruletypeid = models.AutoField(db_column='RuleTypeId', primary_key=True)
    subreddit = models.ForeignKey(
        'Subreddit', db_column='SubredditId', on_delete=models.DO_NOTHING)
    name = models.CharField(db_column='Name', max_length=20)
    points = models.IntegerField(db_column='Points')
    version = models.IntegerField(db_column='Version', default=1, null=True, blank=True)

    def __str__(self):
        return f'[Ruletype {self.pk}/{self.name}]'

    class Meta:
        db_table = 'moderation_RuleType'


class Ruletypehist(models.Model):
    """ unclear """
    ruletype = models.ForeignKey(
        'Ruletype', db_column='RuleTypeId', on_delete=models.DO_NOTHING)
    version = models.IntegerField(db_column='Version')
    subreddit = models.ForeignKey(
        'Subreddit', db_column='SubredditId', on_delete=models.DO_NOTHING)
    name = models.CharField(db_column='Name', max_length=20)
    points = models.IntegerField(db_column='Points')

    class Meta:
        db_table = 'moderation_RuleTypeHist'


class Ruleviolationhist(models.Model):
    """ tracks rule violations """
    ruleviolationhistid = models.AutoField(
        db_column='RuleViolationHistId', primary_key=True)
    ruletype = models.ForeignKey(
        'Ruletype', db_column='RuleTypeId', on_delete=models.DO_NOTHING)
    submissioncomment = models.ForeignKey(
        'Submissioncomment', db_column='SubmissionCommentId',
        on_delete=models.DO_NOTHING)
    moduser = models.ForeignKey(
        'Reddituser', db_column='ModUserId', on_delete=models.DO_NOTHING)
    iscommentmodrestored = models.BooleanField(
        db_column='IsCommentModRestored', blank=True, null=True)
    commentmodrestoreddttm = models.DateTimeField(
        db_column='CommentModRestoredDtTm', blank=True, null=True)
    modt1id = models.CharField(
        db_column='ModT1Id', max_length=20, blank=True, null=True)
    ruletypeversion = models.IntegerField(db_column='RuleTypeVersion', null=True, blank=True)

    def __str__(self):
        return f'RVH ({self.pk}) / {self.submissioncomment.reddituser.username}'

    class Meta:
        db_table = 'neutralverse_RuleViolationHist'


class Submission(models.Model):
    """ tracks post submissions """
    submissionid = models.AutoField(db_column='SubmissionId', primary_key=True)
    subreddit = models.ForeignKey(
        'Subreddit', db_column='SubredditId', on_delete=models.DO_NOTHING)
    reddituser = models.ForeignKey(
        'Reddituser', db_column='RedditUserId', on_delete=models.DO_NOTHING)
    t3id = models.CharField(
        db_column='T3Id', max_length=20, blank=True, null=True)
    permalink = models.CharField(db_column='Permalink', max_length=255)
    isremoved = models.BooleanField(db_column='IsRemoved')
    submitteddttm = models.DateTimeField(db_column='SubmittedDtTm', default=timezone.now)

    class Meta:
        db_table = 'neutralverse_Submission'


class Submissioncomment(models.Model):
    """ tracks user comments """
    submissioncommentid = models.AutoField(
        db_column='SubmissionCommentId', primary_key=True)
    subreddit = models.ForeignKey(
        'Subreddit', db_column='SubredditId', on_delete=models.DO_NOTHING)
    reddituser = models.ForeignKey(
        'Reddituser', db_column='RedditUserId', on_delete=models.DO_NOTHING)
    t1id = models.CharField(
        db_column='T1Id', max_length=20, blank=True, null=True)
    firstcommentid = models.CharField(
        db_column='FirstCommentId', max_length=20)
    permalink = models.CharField(
        db_column='Permalink', max_length=255, blank=True, null=True)
    submitteddttm = models.DateTimeField(
        db_column='SubmittedDtTm', default=timezone.now)
    lastupdatedttm = models.DateTimeField(
        db_column='LastUpdateDtTm', blank=True, null=True, default=timezone.now)

    def __str__(self):
        return f'SC ({self.pk}) / {self.reddituser.username}'

    class Meta:
        db_table = 'neutralverse_SubmissionComment'


class Violationssettings(models.Model):
    """ TODO unclear """
    violationssettingsid = models.AutoField(
        db_column='ViolationsSettingsId', primary_key=True)
    subreddit = models.ForeignKey(
        'Subreddit', db_column='SubredditId', on_delete=models.DO_NOTHING)
    trackdurationdays = models.IntegerField(db_column='TrackDurationDays')
    restorerefundpercent = models.IntegerField(
        db_column='RestoreRefundPercent')
    deleterefundpercent = models.IntegerField(
        db_column='DeleteRefundPercent')
    aggregateviolationsincomment = models.BooleanField(
        db_column='AggregateViolationsInComment')
    aggregateviolationsinchain = models.BooleanField(
        db_column='AggregateViolationsInChain')

    class Meta:
        db_table = 'moderation_ViolationsSettings'
