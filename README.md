# neutralverse

### Development environment setup

Project uses python 3.8

To create an isolated environment for the project, use `python -m venv`

A makefile is included which will create one at .venv and add the project dependencies
from requirements.txt

#### Note for macOS

In order to use the `pymssql` package, make sure `FreeTDS` is installed on your system:
https://pymssql.readthedocs.io/en/stable/intro.html

### Testing

Tests should be colocated with their implementation, e.g.:

```
actions/
  auto_poster.py
  auto_poster_test.py
```

To run the tests, run `pytest` in your terminal.