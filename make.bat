@echo off

if [%1] == [] goto help

setlocal ENABLEDELAYEDEXPANSION

goto %1

:newenv
py -3.8 -m venv --clear .venv
.\.venv\Scripts\python -m pip install -U pip setuptools
.\.venv\Scripts\python -m pip install -Ur requirements.txt
goto syncenv

:syncenv
.\.venv\Scripts\python -m pip install -Ur requirements.txt
exit /B %ERRORLEVEL%

:help
echo Usage:
echo   make ^<command^>
echo.
echo Commands:
echo   newenv                     Create or replace this project's virtual environment.
echo   syncenv                    Sync this project's virtual environment to Red's latest
echo                              dependencies.
